# eazydiner_tester.py

from selenium import webdriver
from pages.login_page import LoginPage
from pages.search_page import SearchPage
from pages.booking_page import BookingPage
from pages.payeazy_page import PayEazyPage
from pages.prime_page import PrimePage
from pages.base_page import BasePage
from pages.offers_page import OffersPage
import os
import pandas as pd
import time

class EazyDinerTester:

    # def __init__(self, driver=None, base_url='https://new-react-test.easydiner.com/'):
    #     self.base_url = base_url
    #     self.base_page = None
    #     self.login_page = None
    #     self.search_page = None
    #     self.booking_page = None
    #     self.payeazy_page = None
    #     self.prime_page = None
    #     self.offers_page = None
    #     self.driver = None

    # def setup(self, browser):
    #     if browser == 0:
    #         self.driver = webdriver.Chrome()
    #         print("Test running in *Chrome Browser* ")
    #     elif browser == 1:
    #         self.driver = webdriver.Firefox()
    #         print("Test running in *Firefox Browser* ")
    #     elif browser == 2:
    #         self.driver = webdriver.Edge()
    #         print("Test running in *Edge Browser* ")
    #     else:
    #         print("Invalid browser choice. Defaulting to Chrome.")
    #         self.driver = webdriver.Chrome()

    #     self.base_page = BasePage(self.driver)
    #     self.login_page = LoginPage(self.driver)
    #     self.search_page = SearchPage(self.driver)
    #     self.booking_page = BookingPage(self.driver)
    #     self.payeazy_page = PayEazyPage(self.driver)
    #     self.prime_page = PrimePage(self.driver)
    #     self.offers_page = OffersPage(self.driver)
    #     self.driver.maximize_window()

    def __init__(self, driver=None, base_url='https://www.eazydiner.com/'):
        self.driver = driver or webdriver.Chrome()
        # print()
        # browser = input("\nEnter the browser you want to use (chrome/firefox/edge): ").lower()
        # if browser == "chrome":
        #     self.driver = webdriver.Chrome()
        # elif browser == "firefox":
        #     self.driver = webdriver.Firefox()
        # elif browser == "edge":
        #     self.driver = webdriver.Edge()
        # else:
        #     print("Invalid browser choice. Defaulting to Chrome.")
        #     self.driver = webdriver.Chrome()
        self.base_url = base_url
        self.base_page = BasePage(self.driver)
        self.login_page = LoginPage(self.driver)
        self.search_page = SearchPage(self.driver)
        self.booking_page = BookingPage(self.driver)
        self.payeazy_page = PayEazyPage(self.driver)
        self.prime_page = PrimePage(self.driver)
        self.offers_page = OffersPage(self.driver)

    def setup(self):
        print("\n***********************Test started****************************\n ")
        self.driver.maximize_window()

    def go_to_url(self, url):
        self.driver.get(url)
        print(f"Navigated to {url}")


    def teardown(self):
        if self.driver:
            self.driver.quit()
        print("\n***********************Test ended****************************\n ")
    def start(self):
        self.base_page.open_website()


    def take_screenshot(self, filename):
        screenshots_dir = 'screenshots' 
        if not os.path.exists(screenshots_dir):
            os.makedirs(screenshots_dir)
        filepath = os.path.join(screenshots_dir, filename)
        self.driver.save_screenshot(filepath)
        return filepath

    def capture_error_screenshot(self, error_message):  
        filename = f'{error_message}.png'
        filepath = self.take_screenshot(filename)
        print(f"Screenshot captured: {filepath}")
        print(f"Error message: {error_message}")

    def go_to_home_page(self):
        self.base_page.navigate_home()

    def refresh(self):
        self.driver.refresh()
    
    def click_login(self):
        self.base_page.click_login_button()

    def login(self, mobile_number,otp):
        self.login_page.enter_mobile_number(mobile_number)

        self.login_page.click_get_otp_button()
        self.login_page.enter_otp(otp)

    def login_enter_number(self,mobile_number):
        self.login_page.enter_mobile_number(mobile_number)
        self.login_page.click_getotp_button()

    def login_enter_otp(self,otp):
        self.login_page.enter_otp(otp)

    def newUser_login(self,mobile_number,name,email,dob):
        self.login_page.enter_mobile_number(mobile_number)
        self.login_page.click_getotp_button()
        self.login_page.enter_name(name)
        self.login_page.enter_email(email) 
        self.login_page.click_dob()
        self.login_page.enter_dob(dob)
        
    def resend_otp(self):
        self.login_page.click_resend_otp_button()


    def open_search_tab(self):
        self.base_page.open_search_tab()

    def is_search_content(self):
        return self.search_page.check_page_content()
    
    def search_restaurant(self, restaurant_name):
        self.search_page.enter_search(restaurant_name)

    def select_restaurant(self, restaurant_name):    
        self.search_page.click_res(restaurant_name)  

    def select_res_by_location(self, query):
        self.search_page.click_res_by_location(query)

    def select_chain_res(self):
        self.search_page.select_chain_res()

    def is_list_showing(self):
        return self.search_page.check_list()
    def is_selected(self, restaurant_name):
        title=self.driver.title
        return self.search_page.check_selected(title.split(',',1)[0],restaurant_name)
    def is_recent_showing(self,restaurant_name):
        return self.search_page.check_recent(restaurant_name)

    def select_slot(self,date):
        self.booking_page.open_slot_tab()
        self.booking_page.select_slots_date(date)

    def click_find_best_offer(self):
        self.booking_page.click_find_best_offer()

    def open_slot_tab(self):
        self.booking_page.open_slot_tab()
    
    def select_guest(self, guest):
        self.booking_page.guest_number(guest)

    def is_guest_popup_visible(self):
        return self.booking_page.check_guest_popup()

    def book(self):   
        self.booking_page.click_book_button()
        self.booking_page.submit_booking()

    def free_book(self):
        self.booking_page.find_free()

    def is_redirected_juspay(self):    
        redirected_url = self.driver.current_url  
        return self.prime_page.check_juspay_page(redirected_url)

    def is_unbookable_error(self):
        return self.booking_page.check_restaurant_bookable()
    def is_slot_date_correct(self):
        self.booking_page.open_slot_tab()
        return self.booking_page.check_slot_date()

    def is_slot_time_correct(self):
        self.booking_page.open_slot_tab()
        return self.booking_page.check_slot_time()
    
    def open_deal_offers(self,offer):
        self.booking_page.open_offer_popup(offer)

    def open_voucher_tab(self):
        self.booking_page.click_vouchers()
    
    def add_vouchers(self):
        self.booking_page.find_add()
    
    def is_active_tab_opened(self,offer):
        return self.booking_page.check_active_offer(offer)
    
        
    def is_juspay_element_visible(self):
        if self.payeazy_page.check_juspay_card()==False or self.payeazy_page.check_juspay_netbanking()==False or self.payeazy_page.check_juspay_wallet()==False or self.payeazy_page.check_juspay_upi()==False:
            return False
        else:
            return True


    def is_calculate_discount_correct(self):
        return self.booking_page.calculate_discount(0)
    
    def is_convenience_fee_correct(self):
        return self.payeazy_page.check_convenience_fee()
    

    def pay_bill(self):    
        self.booking_page.click_pay_bill()

    def pay_bill_banner(self):
        self.booking_page.click_pay_bill_banner()

    def check_offers_and_payment_mode(self):
        return self.payeazy_page.check_net_payable()
        # return self.payeazy_page.check_card_juspay()
    
    def check_card_bin(self):
        return self.payeazy_page.check_card_juspay()
    

    def check_coupons(self):
        coupons_webpage=self.offers_page.get_coupons_code()
        coupons_csv=self.offers_page.get_coupons_from_csv()
        coupons_webpage.sort()
        coupons_csv.sort()
        print("coupons present on webpage:\n",coupons_webpage)
        print("******************************************")
        print("coupons present in csv file:\n",coupons_csv)
        if coupons_webpage!=coupons_csv:
            print(f"coupon from webpage and csv file doesn't match")
            return False
        else:
            print(f"coupon from csv file and webpage matches")
            return True
            

    def check_offer_count(self):
        offers_count_displayed = self.payeazy_page.get_offer_count()
        offers_visible=self.payeazy_page.count_offers()
        if offers_count_displayed == offers_visible:
            print("correct number of offers displayed")
            return True
        else:
            return False
        
    def check_extra_offers(self):
        return self.payeazy_page.check_extra_offers()
        
        

    def pay_bill_bbq(self):
        self.booking_page.click_payBill_bbq()
        
    def is_redirected_payeazy(self):
        redirected_url= self.driver.current_url
        return self.payeazy_page.check_payeazy_page(redirected_url)
    
    def is_convenience_fee_correct(self):
        return self.payeazy_page.check_convenience_fee()

                                             

    def payeazy(self,amount):    
        self.payeazy_page.enter_amount(amount)
        self.payeazy_page.click_check_offers()
        self.payeazy_page.check_error()


    def check_result(self,amount):
        self.offers_page.check_error(amount)

    def location(self,location):
        self.base_page.location(location)
        

    def logout(self):
        self.base_page.navigate_to_profile()
        self.base_page.logout()
        
    def is_loggedin(self):
        return self.login_page.check_login()
    
    def is_loggedout(self):
        return self.login_page.check_logout()
        
    def click_prime(self):
        self.base_page.click_prime_button()

    def click_proceed(self):
        self.prime_page.click_proceed_button()
    
    def click_cross(self):
        self.prime_page.click_cross() 

    def click_select_payment_option(self):
        self.prime_page.click_select_payment_button()

    def confirm_payable_amount(self):
        initial_amount = self.prime_page.get_amount()
        gst_amount = self.prime_page.gst_amount(initial_amount)
        assert self.prime_page.compare_amount(initial_amount + gst_amount), "Payable amount isn't correct"
        print("Payable amount is correct")

    def confirm_payable_amount_prev_mem(self):
        initial_amount = self.prime_page.get_amount_prevMem()
        gst_amount = self.prime_page.gst_amount(initial_amount)
        assert self.prime_page.compare_amount_prev_mem(initial_amount + gst_amount), "Payable amount isn't correct"
        print("Payable amount is correct")

    def confirm_payable_amount_one_month(self):
        initial_amount = self.prime_page.get_amount_one_month()
        gst_amount = self.prime_page.gst_amount(initial_amount)
        assert self.prime_page.compare_amount_one_month(initial_amount + gst_amount), "Payable amount isn't correct"
        print("Payable amount is correct")

    def confirm_payable_amount_one_month_prevMem(self):
        initial_amount = self.prime_page.get_amount_one_month_prevMem()
        gst_amount = self.prime_page.gst_amount(initial_amount)
        assert self.prime_page.compare_amount_one_month_prevMem(initial_amount + gst_amount), "Payable amount isn't correct"
        print("Payable amount is correct")
    
    def confirm_payable_amount_yearly(self):
        initial_amount = self.prime_page.get_amount_yearly()
        gst_amount = self.prime_page.gst_amount(initial_amount)
        assert self.prime_page.compare_amount_yearly(initial_amount + gst_amount), "Payable amount isn't correct"
        print("Payable amount is correct")

    def confirm_payable_amount_yearly_prevMem(self):
        initial_amount = self.prime_page.get_amount_yearly_prevMem()
        gst_amount = self.prime_page.gst_amount(initial_amount)
        assert self.prime_page.compare_amount_yearly_prevMem(initial_amount + gst_amount), "Payable amount isn't correct"
        print("Payable amount is correct")

    def click_one_month_plan(self):
        self.prime_page.click_monthly_option()

    def click_3month_plan(self):
        self.prime_page.click_three_month_plan()
    
    def click_one_month_plan_prevMem(self):
        self.prime_page.click_monthly_option_prev_mem()

    def click_yearly_plan(self):
        self.prime_page.click_yearly_plan_option()

    def click_yearly_plan_prevMem(self):
        self.prime_page.click_yearly_plan_option_prevMem()

    def book_a_table(self):
        self.base_page.click_bookATable()

    def click_first_restaurant(self):
        self.offers_page.click_firstRestaurant()

    def click_enter_coupon_code(self,offer):
        self.offers_page.enter_coupon_code(offer)
    
    def click_apply(self):
        self.offers_page.click_apply_button()

    def click_Ok(self):
        self.offers_page.click_ok_button()

    def redirect_payeazy_page(self):
        self.driver.get("https://new-react-test.easydiner.com/payeazy-payment/delhi-ncr/the-cuisine-lab-dlf-phase-3-gurgaon-689375")

    def close_offers_popup(self):
        self.offers_page.click_cross_button()

    def check_Convenience_fee(self):
        assert self.offers_page.convenience_fee(),"convenience fee applied before using any offer."

    def check_payable_value(self):
        assert self.offers_page.check_netPayable()
        
    def check_amount_yearly_plan(self):
        return self.prime_page.match_offer_amount_yearly_plan()
    
    def check_amount_monthly_plan(self):
        return self.prime_page.match_offer_amount_monthly_plan()
    
    def check_amount_three_month_plan(self):
        return self.prime_page.match_offer_amount_3month_plan()
    
    def check_amount_3month_plan_prevMem(self):
        return self.prime_page.match_offer_amount_3month_plan_prevMem()
    
    def check_amount_yearly_plan_prevMem(self):
        return self.prime_page.match_offer_amount_yearly_plan_prevMem()
    
    def check_amount_monthly_plan_prevMem(self):
        return self.prime_page.match_offer_amount_monthly_plan_prevMem()
    
    def is_redirected_primePage(self):
        redirected_url = self.driver.current_url  
        return self.paymentOffers_page.check_prime_page(redirected_url)
    
    def check_amount_dubai_three_month_plan(self):
        return self.prime_page.match_offer_amount_3month_plan_dubai()
    
    def check_amount_dubai_yearly_plan(self):
        return self.prime_page.match_offer_amount_yearly_plan_dubai()
        
    def check_net_payable_3monthPlan_dubai(self):
        return self.prime_page.match_net_payable_3month_plan_dubai()
    
    def check_net_payable_yearly_Plan_dubai(self):
        return self.prime_page.match_net_payable_yearly_plan_dubai()
    
    def click_yearly_plan_dubai(self):
        return self.prime_page.select_yearly_plan_dubai()
    
    def navigateBack(self):
        return self.base_page.navigate_back()
    
    def match_available_offers(self):
        return self.offers_page.match_offers()
    
    def check_offers_and_netPayable(self):
        return self.offers_page.check_net_payable()
    
    def select_payment_way(self):
        return self.prime_page.click_payment_method()
    
    def is_redirected_telr(self):
        redirected_url = self.driver.current_url  
        print(redirected_url)
        return self.prime_page.check_telr_page(redirected_url)
    

        
# Example 
# tester = EazyDinerTester()
# tester.setup()
# (your process)
# tester.teardown()