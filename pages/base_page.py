# base_page.py
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from utils.locators import *
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.by import By

class BasePage:
    def __init__(self, driver):
        self.driver = driver

    def open_website(self):     
        self.driver.get("https://www.eazydiner.com/")
        print("Opened website")  

    def navigate_home(self):
        self.perform_action(BasePageLocators.HOME_TAB)
        self.wait_for_presence(BasePageLocators.SEARCH_TAB)
        print("Navigated to home")
        time.sleep(3)


    def navigate_back(self):
        self.driver.back()
        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.TAG_NAME, 'body')))
        print("Navigated back")

    def load_page(self):
        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.TAG_NAME, 'body')))

    def perform_action(self, locator, sleep=0, action='click', value=None, maxtries=1, noloop=False):
        try_count = 0
        while try_count < maxtries:
            try:
                element = WebDriverWait(self.driver, 10).until(
                    EC.presence_of_element_located(locator)
                )
                time.sleep(sleep)

                if action == 'click':
                    element.click()
                elif action == 'input':
                    element.send_keys(value)
                elif action == 'submit':
                    element.submit()
                else:
                    raise ValueError(f"Invalid action: {action}")

                return True

            except Exception as e:
                print(f"Error: {e}")
                try_count += 1

                if noloop:
                    break

        return False
    

    def clear_input(self, locator):
        element=self.find_element(locator)
        element.clear()

    def wait_for_clickable(self, locator):
        return WebDriverWait(self.driver, 30).until(EC.element_to_be_clickable(locator))
        # print("element is clickable")
    
    def wait_for_visible(self, locator):
        WebDriverWait(self.driver, 30).until(EC.visibility_of_element_located(locator))
        # print("element is visible.")
    
    def wait_for_presence(self, locator):
        WebDriverWait(self.driver, 30).until(EC.presence_of_element_located(locator))
        # print("element is present")

    def dont_wait_for_presence(self, locator):
        WebDriverWait(self.driver, 30).until_not(EC.presence_of_element_located(locator))

    def click_login_button(self):
        self.wait_for_clickable(BasePageLocators.LOGIN_BUTTON)
        self.perform_action(BasePageLocators.LOGIN_BUTTON)
        print("Clicked on login button")    

    def open_search_tab(self):
        self.wait_for_visible(BasePageLocators.SEARCH_TAB)
        self.perform_action(BasePageLocators.SEARCH_TAB)    
        print("Clicked on search tab")

    def click_prime_button(self):
        self.wait_for_clickable(BasePageLocators.PRIME)
        self.perform_action(BasePageLocators.PRIME)    
        print("Clicked on prime button")
        time.sleep(2)

    def is_element_visible(self, locator):
        try:
            self.find_element(locator)
            return True
        except:
            return False
        
    def scroll_down(self, pixels):
        self.driver.execute_script(f"window.scrollBy(0, {pixels});")
        print("Scrolled down")
        
    def find_element(self, locator):
        return WebDriverWait(self.driver, 30).until(EC.visibility_of_element_located(locator))    

    def find_element_for_timeout(self, locator,time):
        return WebDriverWait(self.driver, time).until(EC.visibility_of_any_elements_located(locator))
    

    def navigate_to_profile(self):
        self.navigate_home()
        self.perform_action(BasePageLocators.USER_BUTTON)
        print("Clicked on user button")
        self.perform_action(BasePageLocators.PROFILE_BUTTON)
        print("Clicked on profile button")    

    def location(self,location):
        self.perform_action(BasePageLocators.LOCATION)
        print("Clicked on location tab")
        self.wait_for_visible(LocationPageLocators.LOCATION_INPUT)
        self.perform_action(LocationPageLocators.LOCATION_INPUT, action='input', value=location)
        print("Entered location")
        time.sleep(2)
        self.perform_action(LocationPageLocators.LOCATION_SELECT(location))
        print("Selected location")    
        time.sleep(3)
    
    def logout(self):
        self.perform_action(ProfilePageLocators.ACTIVITY_BUTTON)
        print("Clicked on activity tab")
        time.sleep(2)
        self.perform_action(ProfilePageLocators.LOG_OUT)    
        print("Clicked on LOG OUT button")
        time.sleep(2)
        WebDriverWait(self.driver, 10).until(EC.alert_is_present())
        print("alert visible")
        alert = self.driver.switch_to.alert
        alert.accept()
        print("alert accepted")
        print("Logged out")

    def find_elements(self, locator):
        return WebDriverWait(self.driver, 10).until(
            EC.visibility_of_all_elements_located((locator))
        )
    def click_bookATable(self):
        self.wait_for_clickable(BasePageLocators.BOOK_A_TABLE)
        self.perform_action(BasePageLocators.BOOK_A_TABLE)    
        print("Clicked on book a table button")
        time.sleep(1)

    


    def open_profile_buttons(self,button):
        element=self.find_element(ProfilePageLocators.PROFILE_BUTTON(button))
        self.perform_action(element)