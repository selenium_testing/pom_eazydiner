# booking_page.py

from .base_page import BasePage
from utils.locators import *
from selenium.webdriver.common.action_chains import ActionChains
import time
from selenium.common.exceptions import TimeoutException
from datetime import datetime, timedelta
import pytz
import math
import re

class BookingPage(BasePage):


    def open_slot_tab(self):
        self.wait_for_presence(BookingPageLocators.SLOT_TAB)
        self.perform_action(BookingPageLocators.SLOT_TAB)
        print("Opened slot tab")
    
    def select_slots_date(self,date):
        self.wait_for_visible(BookingPageLocators.DATE_TAB(date))
        self.perform_action(BookingPageLocators.DATE_TAB(date),action='click')
        print("Clicked on date tab")
        time.sleep(4)
    def click_find_best_offer(self):    
        self.perform_action(BookingPageLocators.FIND_BEST_OFFER_BUTTON)
        print("Clicked on FIND BEST OFFER ")
        time.sleep(2)
        self.scroll_down(150)


    def click_next_deal(self):
        offer_section = self.find_element(BookingPageLocators.OFFER_SECTION)
        ActionChains(self.driver).move_to_element(offer_section).perform()
        self.perform_action(BookingPageLocators.NEXT_DEAL)
        print("Clicked on Next Deal button")
    
    def find_free(self):
        while True:
            if self.is_element_visible(BookingPageLocators.BOOK_FOR_FREE):
                print("Found BOOK FOR FREE button")
                time.sleep(2)
                break
            self.click_next_deal()
            self.click_next_deal()
        self.perform_action(BookingPageLocators.BOOK_FOR_FREE)
        print("Clicked on BOOK FOR FREE button")
        self.wait_for_visible(BookingPageLocators.SUBMIT_BOOKING)

    def click_book_button(self):
        self.wait_for_visible(BookingPageLocators.BOOK_BUTTON)
        self.perform_action(BookingPageLocators.BOOK_BUTTON)
        print("Clicked on BOOK button")
        self.wait_for_visible(BookingPageLocators.SUBMIT_BOOKING)
    
    def click_pay_bill(self):
        try :
            self.wait_for_visible(BookingPageLocators.PAY_BILL)
        except TimeoutException:
            print("PAY BILL button not available")
        
        self.perform_action(BookingPageLocators.PAY_BILL)
        print("Clicked on PAY BILL button")
        time.sleep(3)

    def click_payBill_bbq(self):
        self.perform_action(BookingPageLocators.PAY_BILL_BBQ)
        print("Clicked on PAY BILL button")
        time.sleep(2)
    
    def click_pay_bill_banner(self):
        self.perform_action(BookingPageLocators.PAY_BILL_BANNER)
        print("Clicked on PAY BILL banner")
        # time.sleep(2)

    def submit_booking(self):
        self.wait_for_visible(BookingPageLocators.SUBMIT_BOOKING)
        time.sleep(2)
        self.perform_action(BookingPageLocators.SUBMIT_BOOKING)
        print("Clicked on SUBMIT BOOKING button")
        time.sleep(5)

    def check_juspay(self,redirected_url):
        if "juspay.in" in redirected_url:
            print("Redirected to juspay.in page")
            return True
        else:
            print("Redirection to juspay.in page failed")
            return False

    def booking_status(self):    
        try:
            self.wait_for_presence(BookingPageLocators.CONFIRM_BOOKING)
            print("CONFIRMED BOOKING")
        except TimeoutException:
            print("BOOKING NOT CONFIRMED")

    
    def check_slot_date(self):
        time.sleep(3)
        self.wait_for_visible(BookingPageLocators.SLOT_TAB)
        today = datetime.now().date()
        print(f"current date: {today}") 
        future_date = today + timedelta(days=30)
        future_date_formatted = future_date.strftime('%d %b')
        print(f"Last date: {future_date_formatted}")
        locator = BookingPageLocators.DATE_TAB(future_date_formatted)
        try:
            self.wait_for_visible(locator)
            print(f"Date tab for {future_date_formatted} is available after 30 days")
            return True                                                                              
        except TimeoutException:
            print(f"Date tab for {future_date_formatted} is not available after 30 days")
            return False
        
    def check_slot_time(self):        
        ist = pytz.timezone('Asia/Kolkata')
        first_time_str = self.find_element(BookingPageLocators.ACTIVE_TIME).text
        first_time = datetime.strptime(first_time_str, '%I:%M %p')
        first_time = first_time.time()

        current_time_ist = datetime.now(ist).time()
        print(f"Current time IST: {current_time_ist.strftime('%I:%M %p')}")
        if first_time > current_time_ist:
            print("************Correct slots***************")
            return True
        else:
            print("**************Wrong slots***************")
            return False
        

    def guest_number(self, guest):
        locator=BookingPageLocators.GUEST_TAB(guest)
        self.wait_for_visible(locator)
        self.perform_action(locator)
        time.sleep(3)
        print("Clicked on GUEST TAB")
        
        
    def check_guest_popup(self):
        try :
            self.wait_for_visible(BookingPageLocators.GUEST_POPUP)
            print("GUEST POPUP")
            return True
        except TimeoutException:
            print("NO GUEST POPUP")
            return False
        

    def open_offer_popup(self, offer):
        self.wait_for_visible(BookingPageLocators.OFFER_BUTTON(offer))
        self.perform_action(BookingPageLocators.OFFER_BUTTON(offer))    
        time.sleep(2)
        print("Clicked on OFFER TAB")

    def check_active_offer(self,offer):
        active_offer = self.find_element(BookingPageLocators.POP_UP_OFFER_ACTIVE(offer)).text
        print(active_offer)
        if active_offer==offer:    
            print("CORRECT OFFER ACTIVE")
            return True
        else:
            print("WRONG OFFER ACTIVE")
            return False
        
    def click_vouchers(self):
        self.wait_for_visible(BookingPageLocators.BUY_VOUCHER)
        self.perform_action(BookingPageLocators.BUY_VOUCHER)
        print("Clicked on VOUCHERS button")
        time.sleep(3)

    def find_add(self):
        self.wait_for_visible(BookingPageLocators.CONTINUE_TO_BUY)

        while True:
            if self.is_element_visible(BookingPageLocators.ADD_VOUCHER):
                time.sleep(2)
                break
            self.click_next_deal()
        time.sleep(2)
        self.perform_action(BookingPageLocators.ADD_VOUCHER)
        print("Clicked on ADD button")       


    def calculate_discount(self, percent):
        self.wait_for_visible(BookingPageLocators.DISCOUNT_CALCULATOR)
        self.perform_action(BookingPageLocators.DISCOUNT_CALCULATOR)
        amount = 10000
        self.perform_action(BookingPageLocators.AMOUNT_INPUT, action='input', value=amount)
        print(f"Entered amount: {amount}")

        if percent == 0:
            self.perform_action(BookingPageLocators.OPEN_OFFERS)
            self.perform_action(BookingPageLocators.INPUT_OFFER, action='input', value="AU")
            flat_offer = self.find_element(BookingPageLocators.FLAT_DISCOUNT_OFFER)
            flat_offer_text = flat_offer.text
            flat_discount_value_str = re.search(r'₹(\d+)', flat_offer_text).group(1) 
            flat_discount_value = int(flat_discount_value_str)
            print(f"Flat discount value: {flat_discount_value}")
            self.perform_action(BookingPageLocators.SELECT_OFFER_BUTTON)
            print("Selected flat offer")
            
        self.perform_action(BookingPageLocators.CALCULATE_DISCOUNT)
        print("Clicked on CALCULATE DISCOUNT button")
        time.sleep(2)

        restaurant_discount_element = self.find_element(BookingPageLocators.RESTAURANT_DISCOUNT_VALUE)
        payment_discount_element = self.find_element(BookingPageLocators.PAYMENT_DISCOUNT_VALUE)
        final_bill_value = self.find_element(BookingPageLocators.FINAL_VALUE_AFTER_DISCOUNT)

        restaurant_discount = int(restaurant_discount_element.text.split('₹')[-1].strip().replace('-', ''))
        print(f"Restaurant discount shown: {restaurant_discount}")

        payment_discount = int(payment_discount_element.text.split('₹')[-1].strip().replace('-', ''))
        print(f"Payment discount shown: {payment_discount}")

        final_amount = int(final_bill_value.text.split('₹')[-1].strip().replace('-', ''))
        print(f"Final amount shown: {final_amount}")

        amount_after_r_discount = amount - restaurant_discount

        if percent > 0:
            calculated_payment_discount = math.floor((percent / 100) * amount_after_r_discount)
        else:
            calculated_payment_discount = flat_discount_value

        print(f"Calculated payment discount: {calculated_payment_discount}")

        calculated_final_amount = amount_after_r_discount - calculated_payment_discount
        if percent==0 and calculated_final_amount<0:  # for discount exceeding transaction amount
            calculated_final_amount=1
        print(f"Calculated final amount: {calculated_final_amount}")

        if calculated_payment_discount == payment_discount and final_amount == calculated_final_amount:
            print("CORRECT PAYMENT DISCOUNT CALCULATED")
            return True
        else:
            print("WRONG PAYMENT DISCOUNT CALCULATED")
            return False    
    def check_restaurant_bookable(self):
        try :
            self.wait_for_visible(BookingPageLocators.RESTAURANT_NOT_AVAILABLE_MESSAGE)
            print("RESTAURANT NOT BOOKABLE")
            return True
        except TimeoutException:
            print("RESTAURANT BOOKABLE")
            return False