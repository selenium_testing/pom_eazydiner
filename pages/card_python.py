# import random

# def luhn_checksum(card_number):
#     def digits_of(n):
#         return [int(d) for d in str(n)]
#     digits = digits_of(card_number)
#     odd_digits = digits[-1::-2]
#     even_digits = digits[-2::-2]
#     checksum = 0
#     checksum += sum(odd_digits)
#     for d in even_digits:
#         checksum += sum(digits_of(d*2))
#     return (10 - (checksum % 10)) % 10


# def generate_credit_card_numbers(bin, total_digits):
#     account_number_length = total_digits - len(bin) - 1  
#     account_number = ''.join(random.choice('0123456789') for _ in range(account_number_length))
#     partial_card_number = f"{bin}{account_number}0"  
#     checksum = luhn_checksum(int(partial_card_number))
#     card_number = f"{partial_card_number[:-1]}{checksum}"          
#     return card_number

# generated_card_numbers = generate_credit_card_numbers('40156100', 16)
# print(f"Card number {generated_card_numbers}")



# def generate_credit_card_numbers(bins, count, total_digits):
#     card_numbers = []
#     for bin in bins:
#         for _ in range(count):
#             account_number_length = total_digits - len(bin) - 1 
#             account_number = ''.join(random.choice('0123456789') for _ in range(account_number_length))
#             partial_card_number = f"{bin}{account_number}"
#             checksum = luhn_checksum(int(partial_card_number))
#             card_numbers.append(f"{partial_card_number}{checksum}")
#     return card_numbers



import random

def luhn_checksum(card_number):
    def digits_of(n):
        return [int(d) for d in str(n)]
    digits = digits_of(card_number)
    odd_digits = digits[-1::-2]
    even_digits = digits[-2::-2]
    checksum = 0
    checksum += sum(odd_digits)
    for d in even_digits:
        checksum += sum(digits_of(d*2))
    return (10 - (checksum % 10)) % 10

def generate_credit_card_numbers(bin, total_digits):
    if total_digits < 1 or total_digits > 16:
        raise ValueError("Total digits should be between 1 and 16 inclusive")
    
    account_number_length = total_digits - len(bin) - 1  # Subtract length of BIN and checksum digit
    account_number = ''.join(random.choice('0123456789') for _ in range(account_number_length))
    partial_card_number = f"{bin}{account_number}0"  # Include the specified BIN
    checksum = luhn_checksum(int(partial_card_number))
    card_number = f"{partial_card_number[:-1]}{checksum}"  # Replace last digit with checksum
    return card_number

# Example usage:
# bin = "356137"  
# credit_card = generate_credit_card_numbers(bin, 16)  
# print(credit_card)
