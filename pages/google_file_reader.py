# google_sheets_reader.py

import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd

def read_google_sheets():
    # Define the scope and credentials for accessing Google Sheets
    scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
    creds = ServiceAccountCredentials.from_json_keyfile_name(r'D:\pom_eazydiner-1\key\jovial-inkwell-420705-16666038b1bc.json', scope)

    # Authorize the client and open the spreadsheet
    client = gspread.authorize(creds)
    spreadsheet = client.open_by_url('https://docs.google.com/spreadsheets/d/147J7JK8kySOz6ZJBiF5meD_zAjSs-Q2CLLqxLcP8gD4/edit#gid=1097943396')

    # Get the first worksheet
    worksheet = spreadsheet.get_worksheet(0)

    # Read data from the worksheet into a DataFrame
    data = worksheet.get_all_values()
    df = pd.DataFrame(data[1:], columns=data[0])

    return df
