# login_page.py

import time
from .base_page import BasePage
from utils.locators import *
from selenium.common.exceptions import TimeoutException

class LoginPage(BasePage):
     
    def click_login_button(self):
        self.wait_for_clickable(BasePageLocators.LOGIN_BUTTON)
        self.perform_action(BasePageLocators.LOGIN_BUTTON)
        print("Clicked on login button")
        print(BasePageLocators.LOGIN_BUTTON)

    def click_resend_otp_button(self):
        print("Waiting for resend otp button")
        try :
            self.find_element_for_timeout(LoginPageLocators.RESEND_OTP_BUTTON,65)
            print("Resend otp button found")
        except TimeoutException:
            print("Resend otp button not found")    
        self.perform_action(LoginPageLocators.RESEND_OTP_BUTTON)
        print(LoginPageLocators.RESEND_OTP_BUTTON)
        print("Clicked on resend otp button")

    def enter_mobile_number(self, mobile_number):
        self.wait_for_presence(LoginPageLocators.MOBILE_INPUT)
        self.perform_action(LoginPageLocators.MOBILE_INPUT, action='input', value=mobile_number)
        print(LoginPageLocators.MOBILE_INPUT)
        print("Entered mobile number")

    def click_get_otp_button(self):
        self.wait_for_clickable(LoginPageLocators.GET_OTP_BUTTON)
        self.perform_action(LoginPageLocators.GET_OTP_BUTTON)
        print(LoginPageLocators.GET_OTP_BUTTON)
        print("Clicked on get otp button")
        self.wait_for_presence(LoginPageLocators.OTP_INPUT(0))
        
    def click_getotp_button(self):
        self.wait_for_clickable(LoginPageLocators.GET_OTP_BUTTON)
        self.perform_action(LoginPageLocators.GET_OTP_BUTTON)
        print("Clicked on get otp button")

    def enter_otp(self, otp):
        for index, digit in enumerate(otp):
            otp_input_locator = LoginPageLocators.OTP_INPUT(index)
            self.perform_action(otp_input_locator, action='input', value=digit)
            time.sleep(0.5)

        print("Entered otp")  
        time.sleep(3)  
        
        
    def enter_name(self,name):
        self.wait_for_presence(LoginPageLocators.NAME_INPUT)
        self.perform_action(LoginPageLocators.NAME_INPUT,action='input',value=name)
        print("Entered name.")
        time.sleep(0.5)

    def enter_email(self,email):
        self.wait_for_presence(LoginPageLocators.EMAIL_INPUT)
        self.perform_action(LoginPageLocators.EMAIL_INPUT,action='input',value=email)
        print("Entered email.")
        time.sleep(3)

    def click_dob(self):
        self.wait_for_presence(LoginPageLocators.DOB_INPUT)
        self.wait_for_clickable(LoginPageLocators.DOB_INPUT)
        self.perform_action(LoginPageLocators.DOB_INPUT)
        print("Clicked on Date of Birth Box")

    def enter_dob(self,dob):
        self.wait_for_presence(LoginPageLocators.DOB_INPUT)
        self.perform_action(LoginPageLocators.DOB_INPUT,action='input',value=dob)
        print("Entered Date of Birth.")
        time.sleep(2)

    def enter_anniversaryDate(self,ann_date):
        self.wait_for_presence(LoginPageLocators.ANNIVERSARY_INPUT)
        self.perform_action(LoginPageLocators.ANNIVERSARY_INPUT,action='input',value=ann_date)
        print("Entered Anniversary date.")

    def click_signup_now_button(self):
        self.wait_for_clickable(LoginPageLocators.SIGNUP_NOW_BUTTON)
        self.perform_action(LoginPageLocators.SIGNUP_NOW_BUTTON)
        print("Clicked on signup now button")
        self.wait_for_presence(LoginPageLocators.OTP_INPUT(0))

    def check_login(self):
        try:
            self.wait_for_visible(BasePageLocators.USER_BUTTON)
            print("************ login successful ************")
            return True
        except TimeoutException:
            print("************ login unsuccessful **********")
            return False    
        
    def check_logout(self):
        try:
            self.wait_for_visible(BasePageLocators.LOGIN_BUTTON)
            print("************ logout successful ************")
            return True
        except TimeoutException:
            print("************ logout unsuccessful **********")
            return False
