# def check_net_payable1(self):
#         file_path = 'tests/coupons.csv'
#         df = pd.read_csv(file_path)

#         flag = True
#         for index, row in df.iterrows():
#             coupon_code = row.get('couponCode')
#             min_txn_amt = int(row.get('minTxnAmt', 0))
#             discount_type = row.get('discountType', 'percent')
#             discount = int(row.get('discount', 0))
#             max_discount = int(row.get('maxDiscount', 0))
#             c_fee = self.get_convenience_fee(min_txn_amt)
#             amount = min_txn_amt + c_fee

#             if discount_type == 'percent':
#                 discount_value = math.floor((discount / 100) * amount)
#             else:
#                 discount_value = discount

#             if discount_value > max_discount:
#                 discount_value = max_discount

#             net_payable = amount - discount_value
#             # self.wait_for_visible(PayEazyLocators.AMOUNT_INPUT)
#             # self.perform_action(PayEazyLocators.AMOUNT_INPUT, action='input', value=min_txn_amt)
#             self.enter_amount(min_txn_amt)
#             self.click_check_offers()
#             self.wait_for_visible(PayEazyLocators.OFFER_ELEMENTS)
#             self.perform_action(RestaurantLocators.ENTER_OFFER,action='input', value = coupon_code)
#             self.perform_action(RestaurantLocators.APPLY)
#             time.sleep(2)
#             self.wait_for_clickable(RestaurantLocators.OK_THANKS)
#             self.perform_action(RestaurantLocators.OK_THANKS)

#             net_payable_element = self.find_element(PayEazyLocators.NET_PAYABLE)
#             net_payable_text = net_payable_element.text
#             displayed_net_payable = int(net_payable_text.split(' ')[1])
#             print(f"-Expected_net_payable : {net_payable} ")
#             print(f"Displayed_net_payable : {displayed_net_payable}")
#             if net_payable == displayed_net_payable:
#                 print(f"************** Row {index + 1}: Pass **************")
#             else:
#                 flag = False
#                 print(f"************** Row {index + 1}: Fail **************")

#             self.navigate_back()

#         if flag:
#             print("All rows passed")
#             return True
#         else:
#             return False
        





        # for convienence fee adding after discount
    
    # def check_net_payable(self):
    #     file_path = '../tests/coupons.csv'
    #     df = pd.read_csv(file_path)

    #     flag = True
    #     for index, row in df.iterrows():
    #         coupon_code = row.get('couponCode')
    #         min_txn_amt = int(row.get('minTxnAmt', 0))
    #         discount_type = row.get('discountType', 'percent')
    #         discount = int(row.get('discount', 0))
    #         max_discount = int(row.get('maxDiscount', 0))

    #         if discount_type == 'percent':
    #             discount_value = (discount / 100) * min_txn_amt
    #         else:
    #             discount_value = discount

    #         if discount_value > max_discount:
    #             discount_value = max_discount

    #         amount = min_txn_amt - discount_value
    #         self.wait_for_visible(PayEazyLocators.AMOUNT_INPUT)
    #         self.perform_action(PayEazyLocators.AMOUNT_INPUT, action='input', value=min_txn_amt)
    #         self.perform_action(PayEazyLocators.CHECK_OFFERS)
    #         self.wait_for_visible(PayEazyLocators.OFFER_ELEMENTS)
    #         self.enter_coupon_code(coupon_code)
    #         self.click_apply_button()
    #         self.wait_for_clickable(RestaurantLocators.OK_THANKS)
    #         self.click_ok_button()
    #         c_fee = self.get_convenience_fee(min_txn_amt)
    #         net_payable = amount + c_fee

    #         net_payable_element = self.find_element(PayEazyLocators.NET_PAYABLE)
    #         net_payable_text = net_payable_element.text
    #         displayed_net_payable = int(net_payable_text.split(' ')[1])
    #         print("net payable:", net_payable)
    #         print("displayed net payable:", displayed_net_payable)
    #         if net_payable == displayed_net_payable:
    #             print(f"**************Row {index + 1}: Pass**************")
    #         else:
    #             flag = False
    #             print(f"**************Row {index + 1}: Fail**************")

    #         self.navigate_back()

    #     if flag:
    #         print("All rows passed")
    #         return True
    #     else:
    #         return False


#optimised

# def check_net_payable(self):
    #         df = read_google_sheets()
    #         flag = True
            
    #         for index, row in df.iterrows():
    #             if index<9:
    #                 continue
    #             coupon_code = row.get('Offer Code', '')
    #             print(f"Testing coupon: {coupon_code}")

    #             # Skip certain coupon codes
    #             if coupon_code in ['CREDUPI', '', 'MASTERCARD'] or 'INDUSIND' in coupon_code or 'AXBDAY' in coupon_code or 'DBSDC500' in coupon_code or 'DBSI750' in coupon_code:
    #                 continue
                
    #             desc = row.get('Offer Details', '').lower()
    #             min_txn_amt = max(int(row.get('MOV', '1000')), 50)
    #             max_discount = int(row.get('Max Disc Limit', '1000000'))
    #             discount = int(row.get('Disc Percent / Flat', '0'))
                
    #             discount_type = 'percent' if discount > 0 else 'flat'
    #             discount = min(discount, max_discount) if discount_type == 'percent' else max_discount

    #             c_fee = self.get_convenience_fee(min_txn_amt)
    #             amount = min_txn_amt + c_fee

    #             if discount_type == 'flat' and amount < discount:
    #                 discount = amount - 1
    #             elif discount_type == 'percent':
    #                 discount = math.floor((discount / 100) * amount)

    #             if 'cashback' in desc:
    #                 print("CASHBACK OFFER")
    #                 discount = 0

    #             net_payable = amount - discount

    #             # Perform actions with Selenium
    #             self.enter_amount(min_txn_amt)
    #             self.click_check_offers()
    #             self.wait_for_visible(PayEazyLocators.OFFER_ELEMENTS)
    #             self.perform_action(RestaurantLocators.ENTER_OFFER, action='input', value=coupon_code)
    #             self.perform_action(RestaurantLocators.APPLY)
    #             time.sleep(2)
    #             self.wait_for_clickable(RestaurantLocators.OK_THANKS)
    #             self.perform_action(RestaurantLocators.OK_THANKS)

    #             net_payable_element = self.find_element(PayEazyLocators.NET_PAYABLE)
    #             displayed_net_payable = int(net_payable_element.text.split(' ')[1])

    #             if net_payable == displayed_net_payable:
    #                 print(f"************** Row {index + 1} NET PAYABLE: Pass **************")
    #             else:
    #                 print(f"displayed net payable: {displayed_net_payable} and net payable: {net_payable} for coupon: {coupon_code}")
    #                 flag = False
    #                 print(f"************** Row {index + 1} NET PAYABLE: Fail ************************")

    #             # Check payment method
    #             self.perform_action(PayEazyLocators.SELECT_PAYMENT_OPTION)
    #             time.sleep(3)

    #             payment_type = row.get('Card Type', 'UPI').lower()
    #             if 'card' in payment_type.lower():
    #                 card=self.find_element(PayEazyLocators.JUSPAY_CARD).text
    #                 if card:
    #                     print(f"************** Row {index + 1} PAYMENT METHOD: Pass **************")
    #                 else:
    #                     flag = False
    #                     print(f"************** Row {index + 1} PAYMENT METHOD: Fail ************************")
    #             elif 'wallet' in payment_type.lower():
    #                 wallet=self.find_element(PayEazyLocators.JUSPAY_WALLETS).text
    #                 if wallet:
    #                     print(f"************** Row {index + 1} PAYMENT METHOD: Pass **************")
    #                 else:
    #                     flag = False
    #                     print(f"************** Row {index + 1} PAYMENT METHOD: Fail ************************")
    #             else:
    #                 upi=self.find_element(PayEazyLocators.JUSPAY_UPI).text
    #                 if upi:
    #                     print(f"************** Row {index + 1} PAYMENT METHOD: Pass **************")
    #                 else:
    #                     flag = False
    #                     print(f"************** Row {index + 1} PAYMENT METHOD: Fail ************************")

                
    #             time.sleep(3)
    #             self.navigate_back()
    #             self.wait_for_visible(PayEazyLocators.CLOSE_OFFERS_SECTION)
    #             self.perform_action(PayEazyLocators.CLOSE_OFFERS_SECTION)
    #             self.navigate_back()

    #         if flag:
    #             print("All rows passed")
    #             return True
    #         else:
    #             return False




