from .base_page import BasePage
from utils.locators import *
from selenium.webdriver.common.action_chains import ActionChains
import time
from selenium.common.exceptions import TimeoutException
from .google_file_reader import read_google_sheets

import pandas as pd
import math
import numpy as np

class OffersPage(BasePage):

    def enter_coupon_code(self,offer):
        self.wait_for_visible(RestaurantLocators.ENTER_OFFER)
        self.perform_action(RestaurantLocators.ENTER_OFFER, action='input', value=offer)
        print("offer is inputted")
        time.sleep(1)

    def check_error(self, amount):
        try:
            if amount < 2500:
                self.wait_for_visible(RestaurantLocators.OFFER_ERROR)
                print("Error Visible")
            else:
                print("No error visible")
        except:
            assert amount >= 2500, "Error should not be visible for amounts less than or equal to 2500"
            self.wait_for_visible(RestaurantLocators.OK_THANKS)
            self.perform_action(RestaurantLocators.OK_THANKS)
            print("clicked on ok thanks button")
            time.sleep(1)

    def click_firstRestaurant(self):
        self.wait_for_clickable(RestaurantLocators.FIRST_RESTO)
        self.perform_action(RestaurantLocators.FIRST_RESTO)
        print("Clicked on first restaurant")
        time.sleep(1)
    
    def click_apply_button(self):
        self.wait_for_clickable(RestaurantLocators.APPLY)
        self.perform_action(RestaurantLocators.APPLY)
        print("Clicked on apply button")
        time.sleep(1)
    
    def click_cross_button(self):
        self.wait_for_clickable(RestaurantLocators.CROSS)
        self.perform_action(RestaurantLocators.CROSS)
        print("closed offers popup page")
        time.sleep(1)

    def convenience_fee(self):
        try:
            self.wait_for_presence(RestaurantLocators.SELECT_OFFER)
            self.wait_for_presence(RestaurantLocators.CONVENIENCE_FEE)
            return False
        except TimeoutException:
            pass
            
        try:
            self.wait_for_presence(RestaurantLocators.REMOVE_OFFER)
            self.wait_for_presence(RestaurantLocators.CONVENIENCE_FEE)
            return True
        except TimeoutException:
            return False
            
    def click_ok_button(self):
        self.wait_for_clickable(RestaurantLocators.OK_THANKS)
        self.perform_action(RestaurantLocators.OK_THANKS)
        print("click on ok button")
        time.sleep(1)

    def get_coupons_code(self):
        coupons=self.find_elements(OffersLocators.RELATIVE_COUPON_CODE)
        coupons_code=[]
        for coupon in coupons:
            coupons_code.append(coupon.text)
        return coupons_code

    def get_coupons_from_csv(self):
        csv_path='../tests/OFFERS.csv'
        df=pd.read_csv(csv_path)
        # print(df)
        active_coupons=df.loc[df['Status']=='active','Text'].tolist()
        return active_coupons
    
    def get_convenience_fee(self,value):
        convenience_fee_mapping = {
                (0, 499): 10,
                (500, 999): 10,
                (1000, 1499): 25,
                (1500, 1999): 30,
                (2000, 3499): 45,
                (3500, 4999): 75,
                (5000, 7499): 100,
                (7500, 10000): 100,
                (10001, float('inf')): 100  
            }
        for (lower, upper), fee in convenience_fee_mapping.items():
            if lower <= value <= upper:
                return fee
                    
    # for convienence fee adding after discount
    
    # def check_net_payable(self):
    #     file_path = '../tests/coupons.csv'
    #     df = pd.read_csv(file_path)

    #     flag = True
    #     for index, row in df.iterrows():
    #         coupon_code = row.get('couponCode')
    #         min_txn_amt = int(row.get('minTxnAmt', 0))
    #         discount_type = row.get('discountType', 'percent')
    #         discount = int(row.get('discount', 0))
    #         max_discount = int(row.get('maxDiscount', 0))

    #         if discount_type == 'percent':
    #             discount_value = (discount / 100) * min_txn_amt
    #         else:
    #             discount_value = discount

    #         if discount_value > max_discount:
    #             discount_value = max_discount

    #         amount = min_txn_amt - discount_value
    #         self.wait_for_visible(PayEazyLocators.AMOUNT_INPUT)
    #         self.perform_action(PayEazyLocators.AMOUNT_INPUT, action='input', value=min_txn_amt)
    #         self.perform_action(PayEazyLocators.CHECK_OFFERS)
    #         self.wait_for_visible(PayEazyLocators.OFFER_ELEMENTS)
    #         self.enter_coupon_code(coupon_code)
    #         self.click_apply_button()
    #         self.wait_for_clickable(RestaurantLocators.OK_THANKS)
    #         self.click_ok_button()
    #         c_fee = self.get_convenience_fee(min_txn_amt)
    #         net_payable = amount + c_fee

    #         net_payable_element = self.find_element(PayEazyLocators.NET_PAYABLE)
    #         net_payable_text = net_payable_element.text
    #         displayed_net_payable = int(net_payable_text.split(' ')[1])
    #         print("net payable:", net_payable)
    #         print("displayed net payable:", displayed_net_payable)
    #         if net_payable == displayed_net_payable:
    #             print(f"**************Row {index + 1}: Pass**************")
    #         else:
    #             flag = False
    #             print(f"**************Row {index + 1}: Fail**************")

    #         self.navigate_back()

    #     if flag:
    #         print("All rows passed")
    #         return True
    #     else:
    #         return False
            
    def enter_Amount(self,amount):
        self.wait_for_clickable(PayEazyLocators.AMOUNT_INPUT)
        self.perform_action(PayEazyLocators.AMOUNT_INPUT,action='input',value=amount)
        print("Entered amount: ",amount)

    def click_checkOffers(self):
        self.wait_for_clickable(PayEazyLocators.CHECK_OFFERS)
        self.perform_action(PayEazyLocators.CHECK_OFFERS)
        print("clicked on check offer button")
        time.sleep(1)
    
    def check_netPayable(self):
        file_path = '../tests/coupon_convFee.csv'
        df = pd.read_csv(file_path)
        flag = True
        for index, row in df.iterrows():
            coupon_code = row.get('couponCode')
            min_txn_amt = int(row.get('minTxnAmt', 0))
            discount_type = row.get('discountType', 'percent')
            discount = int(row.get('discount', 0))
            max_discount = int(row.get('maxDiscount', 0))
            coupon_fee = self.get_convenience_fee(min_txn_amt)
            total_amount=min_txn_amt+coupon_fee
            print("total amount without discount: ",total_amount)

            if discount_type == 'percent':
                discount_value = math.floor((discount / 100) * total_amount)
                print("discount applied: ",discount_value)
            else:
                discount_value = discount

            if discount_value > max_discount:
                discount_value = max_discount

            net_payable = total_amount - discount_value
            self.enter_Amount(min_txn_amt)
            self.click_checkOffers()
            self.enter_coupon_code(coupon_code)
            self.click_apply_button()
            self.click_ok_button()
            netPayable_element=self.find_element(PayEazyLocators.NET_PAYABLE)
            netPayable_text=int(netPayable_element.text.split(' ')[1])
            print("Expected payable amount: ",net_payable)
            print("Displayed payable amount: ",netPayable_text)
            if net_payable==netPayable_text:
                print("Row ",index+1," Pass")
            else:
                flag=False
                print("Row ",index+1," Fail")    

            # self.navigate_back()

        if flag==True:
            print("All rows passed.")
            return True
        else:
            return False
        
    def match_offers(self):
        self.enter_amount(10000)
        self.click_check_offers()
        offers = self.get_offer_texts()
        csv_texts = self.csv_offers()
        actual_set = set(offers)
        print("actual set is: \n",actual_set)
        csv_set = set(csv_texts)
        print("csv set is :\n",csv_set)
        extra_offers=[]
        if actual_set == csv_set:
            print( "__Offers Matched__" )
            # return True
        else:
            symmetric_diff = actual_set.symmetric_difference(csv_set)
            print("Extra offers found:\n")
            for offer in symmetric_diff:
                if offer in actual_set:
                    extra_offer_info = f"- Extra offer found on webpage: {offer}"
                    print(extra_offer_info)
                    extra_offers.append(offer)
                else:
                    extra_offer_info = f"- Extra offer found in CSV file: {offer}"
                    print(extra_offer_info)
                    extra_offers.append(offer)
                # return False   
        self.perform_action(PayEazyLocators.CLOSE_OFFERS_SECTION)
        return extra_offers
    

    def enter_amount(self, amount):
        self.wait_for_visible(PayEazyLocators.AMOUNT_INPUT)
        self.perform_action(PayEazyLocators.AMOUNT_INPUT, action='input', value=amount)
        print(f"Entered amount: {amount}")

    def click_check_offers(self):
        self.perform_action(PayEazyLocators.CHECK_OFFERS)
        print("Clicked on CHECK OFFERS button")
        # time.sleep(6)
        time.sleep(1)

    def get_offer_texts(self):
        elements = self.find_elements(PayEazyLocators.OFFER_ELEMENTS)
        
        offer_texts = [element.text for element in elements]
        # for text in offer_texts:
        #     print("Offer Text:", text)
        return offer_texts
    
    def csv_offers(self):
        df=read_google_sheets()
        df_limited = df.head(46)
        offers = df_limited.iloc[:, 4].tolist()
        # # for text in offers:
        # #     print("Offer Text:", text)
        return offers
    
    def check_net_payable(self):
            extra_offers=self.match_offers()
            self.navigate_back()
            df=read_google_sheets()
            flag = True
            error_coupons=[]
            for index, row in df.iterrows():
                coupon_code = row.get('Offer Code')
                # if coupon_code=='CREDUPI' or 'INDUSIND' in coupon_code or 'AXBDAY' in coupon_code or coupon_code=='' or 'DBSDC500' in coupon_code or 'DBSI750' in coupon_code or 'MASTERCARD' in coupon_code:
                #     continue

                print(f"Testing coupon : {coupon_code}")
                desc=row.get('Offer Details') #for checking type of payment
                min_txn_amt_str = (row.get('MOV', '1000'))
                payment_type=(row.get('Card Type', 'UPI'))

                if min_txn_amt_str: 
                    min_txn_amt = int(min_txn_amt_str)
                else:
                    min_txn_amt = 1000
                #checking amount entered for avoiding error
                if min_txn_amt<50:
                    min_txn_amt = 100
                self.enter_amount(min_txn_amt)
                self.click_check_offers()
                self.wait_for_visible(PayEazyLocators.OFFER_ELEMENTS)
                self.perform_action(RestaurantLocators.ENTER_OFFER,action='input', value = coupon_code)
                print("Offer inputted")
                self.perform_action(RestaurantLocators.APPLY)
                print("Clicked on apply button.")

                if coupon_code in extra_offers:
                    offer_error = self.find_element(OffersLocators.INAVLID_COUPON_ERROR)
                    if offer_error:
                        print(f"Invalid coupon message displayed for {coupon_code}")
                        print(f"********************* Row {index} : Pass ****************")
                        self.navigate_back()
                    else:
                        print(f"Expected 'Invalid coupon' message not displayed for {coupon_code}")
                        flag = False
                        self.wait_for_clickable(RestaurantLocators.OK_THANKS)
                        self.perform_action(RestaurantLocators.OK_THANKS)
                    continue  # Skip the rest of the logic for extra offers
           
                self.wait_for_clickable(RestaurantLocators.OK_THANKS)
                self.perform_action(RestaurantLocators.OK_THANKS)
                max_discount_str = (row.get('Max Disc Limit', '1000000'))
                if max_discount_str:
                    max_discount = int(max_discount_str)
                else:
                    max_discount = 1000000
                discount_str= (row.get('Disc Percent / Flat', '0'))
                if discount_str:
                    discount = int(discount_str)
                else:   
                    discount = 0

                print(f"Discount : {discount}")
                
                # checking discount type
                if discount>0:
                    discount_type = 'percent'
                else:
                    discount_type = 'flat'
                    discount=max_discount
                c_fee = self.get_convenience_fee(min_txn_amt)
                amount = min_txn_amt + c_fee

                #giving Rs 1 as net payable for flat discount exceeding the amount
                if discount_type=='flat' and amount<discount:
                    discount_value = amount - 1
        
                #normal discount calculating for percent and flat
                elif discount_type == 'percent':
                    discount_value = math.floor((discount / 100) * amount)
                else:
                    discount_value = discount

                #checking max discount
                if discount_value > max_discount:
                    discount_value = max_discount
                print(f"Discount_value : {discount_value}")

                #checking cashback offers
                if 'cashback' in desc.lower():
                   print("CASHBACK OFFER")
                   discount_value=0
                   amount=amount-c_fee

                net_payable = amount - discount_value
                print(f"Amount after conv_fee : {amount}")
                
                net_payable_element = self.find_element(PayEazyLocators.NET_PAYABLE)
                net_payable_text = net_payable_element.text
                displayed_net_payable = int(net_payable_text.split(' ')[1])
                print(f"-Expected_net_payable : {net_payable} ")
                print(f"Displayed_net_payable : {displayed_net_payable}")
                if net_payable == displayed_net_payable:
                    print(f"************** Row {index + 1} NET PAYABLE: Pass **************")
                else:
                    flag = False
                    print(f"************** Row {index + 1} NET PAYABLE: Fail ************************")
                # self.perform_action(PayEazyLocators.SELECT_PAYMENT_OPTION)
                # time.sleep(3)
                # try:
                #     self.wait_for_visible(PayEazyLocators.COUPON_APPLIED)
                # except TimeoutException:
                #     self.navigate_back()
                #     error_coupons.append(coupon_code)
                #     continue

                
                # if 'juspay' in self.driver.current_url.lower():
                #     print("Juspay redirected")
                # else:
                #     flag = False
                #     print("Juspay not redirected")
                    
                
                
                # if 'card' in payment_type.lower():
                #     card=self.find_element(PayEazyLocators.JUSPAY_CARD).text
                #     if card:
                #         print(f"************** Row {index + 1} PAYMENT METHOD: Pass **************")
                #     else:
                #         flag = False
                #         print(f"************** Row {index + 1} PAYMENT METHOD: Fail ************************")
                # elif 'wallet' in payment_type.lower():
                #     wallet=self.find_element(PayEazyLocators.JUSPAY_WALLETS).text
                #     if wallet:
                #         print(f"************** Row {index + 1} PAYMENT METHOD: Pass **************")
                #     else:
                #         flag = False
                #         print(f"************** Row {index + 1} PAYMENT METHOD: Fail ************************")
                # else:
                #     upi=self.find_element(PayEazyLocators.JUSPAY_UPI).text
                #     if upi:
                #         print(f"************** Row {index + 1} PAYMENT METHOD: Pass **************")
                #     else:
                #         flag = False
                #         print(f"************** Row {index + 1} PAYMENT METHOD: Fail ************************")
                
                # time.sleep(3)
                # self.navigate_back()
                # self.wait_for_visible(PayEazyLocators.CLOSE_OFFERS_SECTION)
                # self.perform_action(PayEazyLocators.CLOSE_OFFERS_SECTION)
                # print("Closed offers")
                self.navigate_back()
            
            # for coupon in error_coupons:
            #     print(f"Error coupon : {coupon}")
                
                
            # if flag:
            #     print("All rows passed")
            #     return True
            # else:
            #     return False