from .base_page import BasePage
from utils.locators import *
from selenium.common.exceptions import TimeoutException
import time
from selenium.webdriver.common.by import By
import pandas as pd
import math
import random
import pandas as pd
from .google_file_reader import read_google_sheets
from .card_python import generate_credit_card_numbers


class PayEazyPage(BasePage):

    def check_payeazy_page(self, redirected_url):
        if "payeazy" in redirected_url:
            print("Redirected to payeazy page")
            return True
        else:
            print("Redirection to payeazy page failed")
            return False


    def enter_amount(self, amount):
        self.wait_for_visible(PayEazyLocators.AMOUNT_INPUT)
        self.perform_action(PayEazyLocators.AMOUNT_INPUT, action='input', value=amount)
        print(f"Entered amount: {amount}")

    def click_check_offers(self):
        self.perform_action(PayEazyLocators.CHECK_OFFERS)
        print("Clicked on CHECK OFFERS button")
        # time.sleep(6)
        time.sleep(1)

    def check_error(self):
        try:
            self.wait_for_visible(PayEazyLocators.ERROR_VALUE)
            print("Error Visible")
            time.sleep(1)
            self.perform_action(PayEazyLocators.DISMISS_ERROR)
            print("Clicked on DISMISS button")

        except TimeoutException:
            print("No Error visible")

    
    def get_offer_texts(self):
        elements = self.find_elements(PayEazyLocators.OFFER_ELEMENTS)
        
        offer_texts = [element.text for element in elements]
        # for text in offer_texts:
        #     print("Offer Text:", text)
        return offer_texts
    
    def csv_offers(self):
        df=read_google_sheets()
        df_limited=df.head(46)
        offers = df_limited.iloc[:, 4].tolist()
        # # for text in offers:
        # #     print("Offer Text:", text)
        return offers
        

    def get_offer_count(self):
        offer_count_element = self.find_element(PayEazyLocators.OFFER_COUNT)
        offer_count_text = offer_count_element.text
        offer_count_parts = offer_count_text.split()
        offer_count_number = int(offer_count_parts[0])
        print("Offer count:", offer_count_number)
        return offer_count_number
    
    def count_offers(self):
        elements = self.find_elements(PayEazyLocators.ADDITIONAL_OFFER_NAME)    
        offer_texts = [element.text for element in elements]
        # for text in offer_texts:
        print("Offers visible:", len(offer_texts))
        return len(offer_texts)
    


    def get_convenience_fee(self,value):
        convenience_fee_mapping = {
                (0, 499): 10,
                (500, 999): 20,
                (1000, 1499): 25,
                (1500, 1999): 30,
                (2000, 3499): 45,
                (3500, 4999): 75,
                (5000, 7499): 100,
                (7500, 10000): 100,
                (10001, float('inf')): 100  
            }
        for (lower, upper), fee in convenience_fee_mapping.items():
            if lower <= value <= upper:
                return fee
                    
    
    def check_convenience_fee(self):
        ranges = [
            (0, 499),
            (500, 999),
            (1000, 1499),
            (1500, 1999),
            (2000, 3499),
            (3500, 4999),
            (5000, 7499),
            (7500, 10000),
            (10001, 100000)
        ]

        failed_ranges = []

        for lower, upper in ranges:
            value = random.randint(lower, upper)
            self.enter_amount(value)
            self.click_check_offers()           
            self.wait_for_visible(PayEazyLocators.SELECT_OFFER)
            self.perform_action(PayEazyLocators.SELECT_OFFER)
            print("selected first selectable offer")
            self.perform_action(RestaurantLocators.OK_THANKS)
            expected_fee = self.get_convenience_fee(value)
            actual_fee_element = self.find_element(PayEazyLocators.CONVINIENCE_FEE)
            actual_fee_text = actual_fee_element.text
            actual_fee = int(actual_fee_text.split('₹')[1])
            
            if expected_fee != actual_fee:
                failed_ranges.append((lower, upper, value, expected_fee, actual_fee))
                
            self.navigate_back()

        if failed_ranges:
            for lower, upper, value, expected_fee, actual_fee in failed_ranges:
                print(f"Range ({lower}-{upper}): Fail")
                print(f"Tested Value: {value}, Expected Fee: {expected_fee}, Actual Fee: {actual_fee}")
            return False
        else:
            print("All ranges passed")
            return True

    def check_juspay_card(self):
        try:
            self.find_element(PayEazyLocators.CARD_ELEMENT)
            print("**Juspay CARD element found")
            return True
        except TimeoutException:
            print("**Juspay CARD element not found")
            return False
        
    def check_juspay_netbanking(self):
        try:
            self.find_element(PayEazyLocators.NET_BANKING_ELEMENT)
            print("**Juspay  element found")
            return True
        except TimeoutException:
            print("**Juspay NETBANKING element not found")
            return False
        
    def check_juspay_wallet(self):
        try:
            self.find_element(PayEazyLocators.WALLETS_ELEMENT)
            print("**Juspay WALLET element found")
            return True
        except TimeoutException:
            print("**Juspay WALLET element not found")
            return False
    
    def check_juspay_upi(self):
        try:
            self.find_element(PayEazyLocators.UPI_ELEMENT)
            print("**Juspay UPI element found")
            return True
        except TimeoutException:
            print("**Juspay UPI element not found")
            return False
        
    def check_extra_offers(self):
        self.enter_amount(10000)
        self.click_check_offers()
        offers = self.get_offer_texts()
        
        df = read_google_sheets()
        df = df.iloc[:45]  # Slicing the DataFrame to include only the first 45 rows
        
        csv_texts = df.iloc[:, 4].tolist()
        actual_set = set(offers)
        csv_set = set(csv_texts)
        if actual_set == csv_set:
            print( "****Offers Matched****" )
            
        else:
            extra_offers = actual_set.symmetric_difference(csv_set)
            print("Extra offers found:")
            for offer in extra_offers:
                if offer in actual_set:
                    print("- Extra offer found in webpage:", offer)
            print("**************************")
            for offer in extra_offers:
                if offer in csv_set:
                    print("- Extra offer found in CSV file:", offer)
        return True
        

    def check_net_payable(self):
            # self.check_extra_offers()
            # self.navigate_back()

            df=read_google_sheets()
            flag = True
            error_coupons=[]
            for index, row in df.iterrows():
                if index>46:
                    break
                if index<2:
                    continue
                coupon_code = row.get('Offer Code')
                # if coupon_code=='CREDUPI' or 'INDUSIND' in coupon_code or 'AXBDAY' in coupon_code or coupon_code=='' or 'DBSDC500' in coupon_code or 'DBSI750' in coupon_code or 'MASTERCARD' in coupon_code:
                #     continue
                print(f"Testing coupon : {coupon_code}")
                desc=row.get('Offer Details') #for checking type of payment
                min_txn_amt_str = (row.get('MOV', '1000'))
                payment_type=(row.get('Card Type', 'UPI'))
                if min_txn_amt_str: 
                    min_txn_amt = int(min_txn_amt_str)
                else:
                    min_txn_amt = 1000
                
                #checking amount entered for avoiding error
                if min_txn_amt<50:
                    min_txn_amt = 100

                pay_bill_url=self.driver.current_url
                # print(f"pay_bill_url : {pay_bill_url}")
                self.enter_amount(min_txn_amt)
                self.click_check_offers()
                max_discount_str = (row.get('Max Disc Limit', '1000000'))
                if max_discount_str:
                    max_discount = int(max_discount_str)
                else:
                    max_discount = 1000000
                discount_str= (row.get('Disc Percent / Flat', '0'))
                if discount_str:
                    discount = int(discount_str)
                else:   
                    discount = 0

                print(f"Discount : {discount}")
                
                # checking discount type
                if discount>0:
                    discount_type = 'percent'
                else:
                    discount_type = 'flat'
                    discount=max_discount
                c_fee = self.get_convenience_fee(min_txn_amt)
                amount = min_txn_amt + c_fee

                #giving Rs 1 as net payable for flat discount exceeding the amount
                if discount_type=='flat' and amount<discount:
                    discount_value = amount - 1
        
                #normal discount calculating for percent and flat
                elif discount_type == 'percent':
                    discount_value = math.floor((discount / 100) * amount)
                else:
                    discount_value = discount

                #checking max discount
                if discount_value > max_discount:
                    discount_value = max_discount
                print(f"Discount_value : {discount_value}")

                #checking cashback offers
                if 'cashback' in desc.lower():
                   print("CASHBACK OFFER")
                   discount_value=0
                   amount=amount-c_fee

                net_payable = amount - discount_value
                print(f"Amount after conv_fee : {amount}")
                self.wait_for_visible(PayEazyLocators.OFFER_ELEMENTS)
                self.perform_action(RestaurantLocators.ENTER_OFFER,action='input', value = coupon_code)
                self.perform_action(RestaurantLocators.APPLY)
                time.sleep(2)
                try:
                    offer_error=self.find_element_for_timeout(RestaurantLocators.OK_THANKS,7)
                except TimeoutException:
                    print("~~~~~~~INVALID COUPON CODE~~~~~~~") 
                    error_coupons.append(coupon_code)
                    self.perform_action(PayEazyLocators.CLOSE_OFFERS_SECTION)
                    # self.navigate_back()
                    self.driver.get(pay_bill_url)
                    print("Redirected to pay bill page")
                    continue

                self.wait_for_clickable(RestaurantLocators.OK_THANKS)
                self.perform_action(RestaurantLocators.OK_THANKS)

                net_payable_element = self. find_element(PayEazyLocators.NET_PAYABLE)
                net_payable_text = net_payable_element.text
                displayed_net_payable = int(net_payable_text.split(' ')[1])
                print(f"-Expected_net_payable : {net_payable} ")
                print(f"Displayed_net_payable : {displayed_net_payable}")
                if net_payable == displayed_net_payable:
                    print(f"************** Row {index + 1} NET PAYABLE: Pass **************")
                else:
                    flag = False
                    print(f"************** Row {index + 1} NET PAYABLE: Fail ************************")
                self.perform_action(PayEazyLocators.SELECT_PAYMENT_OPTION)
                time.sleep(3)
                try:
                    self.wait_for_visible(PayEazyLocators.COUPON_APPLIED)
                except TimeoutException:
                    self.navigate_back()
                    error_coupons.append(coupon_code)
                    continue

                
                if 'juspay' in self.driver.current_url.lower():
                    print("Juspay redirected")
                else:
                    flag = False
                    print("Juspay not redirected")
                    
                print(payment_type.lower())
                
                if 'card' in payment_type.lower():
                    card=self.find_element(PayEazyLocators.JUSPAY_CARD).text
                    if card:
                        print(f"************** {coupon_code} Row {index + 1} PAYMENT METHOD: Pass **************")
                    else:
                        flag = False
                        print(f"************** {coupon_code} Row {index + 1} PAYMENT METHOD: Fail ************************")
                elif 'wallet' in payment_type.lower():
                    wallet=self.find_element(PayEazyLocators.JUSPAY_WALLETS).text
                    if wallet:
                        print(f"************** {coupon_code} Row {index + 1} PAYMENT METHOD: Pass **************")
                    else:
                        flag = False
                        print(f"************** {coupon_code} Row {index + 1} PAYMENT METHOD: Fail ************************")
                elif 'net' in payment_type.lower():
                    net=self.find_element(PayEazyLocators.JUSPAY_NET_BANKING).text
                    if net:
                        print(f"************** {coupon_code} Row {index + 1} PAYMENT METHOD: Pass **************")
                    else:
                        flag = False
                        print(f"************** {coupon_code} Row {index + 1} PAYMENT METHOD: Fail ************************")
                else:
                    upi=self.find_element(PayEazyLocators.JUSPAY_UPI).text
                    if upi:
                        print(f"************** {coupon_code} Row {index + 1} PAYMENT METHOD: Pass **************")
                    else:
                        flag = False
                        print(f"************** {coupon_code} Row {index + 1} PAYMENT METHOD: Fail ************************")
                
                time.sleep(3)
                self.navigate_back()
                self.driver.get(pay_bill_url)
                # self.wait_for_visible(PayEazyLocators.CLOSE_OFFERS_SECTION)
                # self.perform_action(PayEazyLocators.CLOSE_OFFERS_SECTION)
                # print("Closed offers")
                # self.navigate_back()
            for coupon in error_coupons:
                print(f"*****Error coupon : {coupon} *****")
                
                
            if flag:
                print("All rows passed")
                return True
            else:
                return False
            
                
        
    
    

    def check_card_juspay(self):
        # Initialize an empty dictionary to store error information
        error_info = {}

        df = read_google_sheets()
        flag = True

        for index, row in df.iterrows():
            # if index<12 or index>12:
            #     continue
            payment_type = row.get('Card Type', 'UPI')

            if 'card' in payment_type.lower():
                coupon_code = row.get('Offer Code')
                print(f"Testing coupon code : {coupon_code}  row: {index + 1}")
                bins = [bin.strip() for bin in row.get('BINS', '').split(',') if bin.strip()]  # Remove leading and trailing spaces
                print(f"Testing bins : {bins}")
                min_txn_amt_str = row.get('MOV', '1000')
                min_txn_amt = int(min_txn_amt_str) if min_txn_amt_str else 1000

                # Check if min_txn_amt is less than 50, set it to 100
                if min_txn_amt < 50:
                    min_txn_amt = 100

                # Enter amount and check offers
                self.enter_amount(min_txn_amt)
                self.click_check_offers()
                self.wait_for_visible(PayEazyLocators.OFFER_ELEMENTS)
                self.perform_action(RestaurantLocators.ENTER_OFFER, action='input', value=coupon_code)
                self.perform_action(RestaurantLocators.APPLY)
                time.sleep(2)
                self.wait_for_clickable(RestaurantLocators.OK_THANKS)
                self.perform_action(RestaurantLocators.OK_THANKS)

                self.perform_action(PayEazyLocators.SELECT_PAYMENT_OPTION)
                time.sleep(3)

                self.wait_for_visible(PayEazyLocators.COUPON_APPLIED)
                if 'juspay' in self.driver.current_url.lower():
                    print("Juspay redirected")
                else:
                    flag = False
                    print("Juspay not redirected")

                self.perform_action(PayEazyLocators.SELECT_CARD)

                # Iterate through bins and check cards
                for bin in bins:
                    print(f"Checking bin : {bin}")
                    dummy_card = generate_credit_card_numbers(bin, 16)
                    print(f"Checking card :{dummy_card}")

                    input_card = self.wait_for_clickable(PayEazyLocators.CARD_INPUT)
                    input_card.clear()
                    input_card.send_keys(dummy_card)
                    time.sleep(2)

                    try:
                        error = self.find_element_for_timeout(PayEazyLocators.CARD_ERROR, 6)
                        if error:
                            print(f"************** Row {index + 1} bin {bin}: Fail **************")
                            error_info[(bin, coupon_code)] = "Fail"
                            flag = False
                    except TimeoutException:
                        print(f"************** Row {index + 1} bin {bin}: pass ************************")

                # Navigate back and close offers section
                self.navigate_back()
                self.wait_for_visible(PayEazyLocators.CLOSE_OFFERS_SECTION)
                self.perform_action(PayEazyLocators.CLOSE_OFFERS_SECTION)
                self.navigate_back()
                self.wait_for_visible(PayEazyLocators.AMOUNT_INPUT)

        # Print error information dictionary
        print("Error Information:")
        for key, value in error_info.items():
            print(f"Bin: {key[0]}, Coupon Code: {key[1]}, Status: {value}")

        if flag:
            print("All bins passed")
            return True
        else:
            return False
        
    def match_offers(self):
        self.enter_amount(10000)
        self.click_check_offers()
        offers = self.get_offer_texts()
        csv_texts = self.csv_offers()
        actual_set = set(offers)
        print("actual set is: \n",actual_set)
        csv_set = set(csv_texts)
        print("csv set is :\n",csv_set)
        if actual_set == csv_set:
            print( "__Offers Matched__" )
            # return True
        else:
            extra_offers = actual_set.symmetric_difference(csv_set)
            print("Extra offers found:/n")
            for offer in extra_offers:
                if offer in actual_set:
                    print("- Extra offer found in webpage:", offer)
                else:
                    print("- Extra offer found in CSV file:", offer)
            # return False   
        self.perform_action(PayEazyLocators.CLOSE_OFFERS_SECTION)





