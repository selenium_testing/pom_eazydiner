from .base_page import BasePage
from utils.locators import *
from selenium.common.exceptions import TimeoutException
import time


class PrimePage(BasePage):

    def check_juspay_page(self, redirected_url):
        assert "juspay" in redirected_url, "Redirection to juspay page failed"
        print("Redirected to juspay page")
        time.sleep(2)
        return True


    def get_amount(self):
        amount_element= self.find_element(PrimeLocators.AMOUNT).text.split('₹')[-1]
        print("amount without gst is",amount_element)
        return float(amount_element)
    
    def get_amount_prevMem(self):
        amount_element= self.find_element(PrimeLocators.AMOUNT_PREV_MEM).text.replace('₹', '').split(':')[1].strip().split(',')[0].strip()
        print("amount without gst is",amount_element)
        return float(amount_element)
    
    def get_amount_one_month(self):
        amount_element= self.find_element(PrimeLocators.AMOUNT_ONE_MONTH).text.split('₹')[-1] 
        print("amount without gst for one month plan is",amount_element)
        return float(amount_element)
    
    def get_amount_one_month_prevMem(self):
        amount_element= self.find_element(PrimeLocators.AMOUNT_1_PREV_MEM).text.replace('₹', '').split(':')[1].strip().split(',')[0].strip()
        print("amount without gst for one month plan is",amount_element)
        return float(amount_element)
    
    def get_amount_yearly(self):
        # amount_element= self.find_element(PrimeLocators.AMOUNT_YEARLY).text.replace('₹', '').split(':')[1].strip().split(',')[0].strip() 
        amount_element= self.find_element(PrimeLocators.AMOUNT_YEARLY).text.split('₹')[-1]
        print("amount without gst for yearly plan is",amount_element)
        return float(amount_element) 
        
    def get_amount_yearly_prevMem(self):
        amount_element= self.find_element(PrimeLocators.AMOUNT_YEARLY_PREV_MEM).text.replace('₹', '').split(':')[1].strip().split(',')[0].strip()
        print("amount without gst for yearly plan is",amount_element)
        return float(amount_element) 
    
    def gst_amount(self,amount):
        gst=0.18
        gstAmount=gst*amount
        # total_payable_amount=amount+gst_amount
        return gstAmount
    
    def compare_amount(self,calculated_amount):
        calculated_amount=int(calculated_amount)
        print("calculated amount is",calculated_amount)
        actual_amount=self.find_element(PrimeLocators.PAYABLE_AMOUNT).text
        actual_amount = ''.join(filter(str.isdigit, actual_amount))
        print("Actual amount is ",actual_amount)
        try:
            assert int(actual_amount) == calculated_amount
            print("Value at locator matches the provided value")
            return True
        except AssertionError:
            print("AssertionError: Value at locator does not match the provided value")
            raise

    def compare_amount_prev_mem(self,calculated_amount):
        calculated_amount=int(calculated_amount)
        print("calculated amount is",calculated_amount)
        actual_amount=self.find_element(PrimeLocators.PAYABLE_AMOUNT_PREV_MEM).text
        actual_amount = ''.join(filter(str.isdigit, actual_amount))
        print("Actual amount is: ",actual_amount)
        try:
            assert int(actual_amount) == calculated_amount
            print("Value at locator matches the provided value")
            return True
        except AssertionError:
            print("AssertionError: Value at locator does not match the provided value")
            raise

    def compare_amount_one_month(self,calculated_amount):
        calculated_amount=int(calculated_amount)
        print("calculated amount is",calculated_amount)
        actual_amount=self.find_element(PrimeLocators.PAYABLE_AMOUNT).text
        actual_amount = ''.join(filter(str.isdigit, actual_amount))
        print("Actual amount is ",actual_amount)
        try:
            assert int(actual_amount) == calculated_amount
            print("Value at locator matches the provided value")
            return True
        except AssertionError:
            print("AssertionError: Value at locator does not match the provided value")
            raise
    
    def compare_amount_one_month_prevMem(self,calculated_amount):
        calculated_amount=int(calculated_amount)
        print("calculated amount is",calculated_amount)
        actual_amount=self.find_element(PrimeLocators.PAYABLE_AMOUNT_ONE_MONTH_PREV_MEM).text
        actual_amount = ''.join(filter(str.isdigit, actual_amount))
        print("Actual amount is : ",actual_amount)
        try:
            assert int(actual_amount) == calculated_amount
            print("Value at locator matches the provided value")
            return True
        except AssertionError:
            print("AssertionError: Value at locator does not match the provided value")
            raise

    def compare_amount_yearly(self,calculated_amount):
        calculated_amount=int(calculated_amount)
        print("calculated amount is",calculated_amount)
        actual_amount=self.find_element(PrimeLocators.PAYABLE_AMOUNT).text
        actual_amount = ''.join(filter(str.isdigit, actual_amount))
        print("Actual amount is ",actual_amount)
        try:
            assert int(actual_amount) == calculated_amount
            print("Value at locator matches the provided value")
            return True
        except AssertionError:
            print("AssertionError: Value at locator does not match the provided value")
            raise 
    
    def compare_amount_yearly_prevMem(self,calculated_amount):
        calculated_amount=int(calculated_amount)
        print("calculated amount is",calculated_amount)
        actual_amount=self.find_element(PrimeLocators.PAYABLE_AMOUNT_YEARLY_PREV_MEM).text
        actual_amount = ''.join(filter(str.isdigit, actual_amount))
        print("Actual amount is : ",actual_amount)
        try:
            assert int(actual_amount) == calculated_amount
            print("Value at locator matches the provided value")
            return True
        except AssertionError:
            print("AssertionError: Value at locator does not match the provided value")
            raise 

    def click_proceed_button(self):
        self.wait_for_clickable(PrimeLocators.PROCEED_BUTTON)
        self.perform_action(PrimeLocators.PROCEED_BUTTON)    
        print("Clicked on proceed button")
        time.sleep(2)
    
    def click_cross(self):
        self.wait_for_clickable(PrimeLocators.CROSS)
        self.perform_action(PrimeLocators.CROSS)    
        print("Clicked on cross")
        time.sleep(2)

    def click_select_payment_button(self):
        self.wait_for_clickable(PrimeLocators.PAYMENT_OPTION)
        self.perform_action(PrimeLocators.PAYMENT_OPTION)    
        print("Clicked on select payment option")
        time.sleep(3)
    
    def click_monthly_option_prev_mem(self):
        self.wait_for_clickable(PrimeLocators.ONE_MONTH_PLAN_PREV_MEM)
        self.perform_action(PrimeLocators.ONE_MONTH_PLAN_PREV_MEM)    
        print("Clicked on one month plan")
        time.sleep(2)

    def click_three_month_plan(self):
        self.wait_for_clickable(PrimeLocators.THREE_MONTH_PLAN)
        self.perform_action(PrimeLocators.THREE_MONTH_PLAN)    
        print("Clicked on three month plan")
        time.sleep(2)
    
    def click_monthly_option(self):
        self.wait_for_clickable(PrimeLocators.ONE_MONTH_PLAN)
        self.perform_action(PrimeLocators.ONE_MONTH_PLAN)    
        print("Clicked on one month plan")
        time.sleep(2)

    def click_yearly_plan_option(self):
        self.wait_for_clickable(PrimeLocators.YEARLY_PLAN)
        self.perform_action(PrimeLocators.YEARLY_PLAN)    
        print("Clicked on yearly plan")
        time.sleep(2)
    
    def click_yearly_plan_option_prevMem(self):
        self.wait_for_clickable(PrimeLocators.YEARLY_PLAN_PREV_MEM)
        self.perform_action(PrimeLocators.YEARLY_PLAN_PREV_MEM)    
        print("Clicked on yearly plan")
        time.sleep(2)

    def match_offer_amount_yearly_plan(self):
        amount = self.find_element(PrimeLocators.YEARLY_PLAN_VALUE).text.split('₹')[-1]
        print("Yearly plan offer amount : ",amount)
        if amount=="2395":
            print("Offer amount is correct")
            return True
        else:
            return False

    def match_offer_amount_monthly_plan(self):
        amount = self.find_element(PrimeLocators.MONTHLY_PLAN_VALUE).text.split('₹')[-1]
        print("monthly plan offer amount : ",amount)
        if amount=="245":
            print("Offer amount is correct")
            return True
        else:
            return False

    def match_offer_amount_3month_plan(self):
        amount = self.find_element(PrimeLocators.THREE_MONTH_PLAN_VALUE).text.split('₹')[-1]
        print("three month plan offer amount : ",amount)
        if amount=="695":
            print("Offer amount is correct")
            return True
        else:
            return False
        
    def match_offer_amount_3month_plan_prevMem(self):
        amount = self.find_element(PrimeLocators.THREE_MONTH_PLAN_VALUE_PREVMEM).text.split('₹')[-1]
        print("three month plan offer amount : ",amount)
        if amount=="499":
            print("Offer amount is correct")
            return True
        else:
            return False
        
    def match_offer_amount_yearly_plan_prevMem(self):
        amount = self.find_element(PrimeLocators.YEARLY_PLAN_VALUE_PREVMEM).text.split('₹')[-1]
        print("Yearly plan offer amount : ",amount)
        if amount=="1499":
            print("Offer amount is correct")
            return True
        else:
            return False
        
    def match_offer_amount_monthly_plan_prevMem(self):
        amount = self.find_element(PrimeLocators.MONTHLY_PLAN_VALUE_PREVMEM).text.split('₹')[-1]
        print("monthly plan offer amount : ",amount)
        if amount=="199":
            print("Offer amount is correct")
            return True
        else:
            return False
        
    def match_offer_amount_3month_plan_dubai(self):
        amount=self.find_element(PrimeLocators.VALUE_THREE_MONTH_PLAN_DUBAI).text.split('AED')[-1]
        print("Three Month plan value for Dubai: ",amount)
        if amount=="40":
            print("Offer amount is correct")
            return True
        else:
            return False
        
    def match_offer_amount_yearly_plan_dubai(self):
        amount=self.find_element(PrimeLocators.VALUE_YEARLY_PLAN_DUBAI).text.split('AED')[-1]
        print("Yearly plan value for Dubai: ",amount)
        if amount=="95":
            print("Offer amount is correct")
            return True
        else:
            return False
        
    def match_net_payable_3month_plan_dubai(self):
        net_payable=self.find_element(PrimeLocators.AMOUNT_THREE_MONTH_DUBAI).text.split('AED')[-1].strip()
        print("net payable amount for 3 month plan in dubai is",net_payable)
        if net_payable=="40":
            print("Net payable for 3 month plan is correct")
            return True
        else:
            print("Net payable is: ",net_payable)
            return False
        
    def match_net_payable_yearly_plan_dubai(self):
        net_payable=self.find_element(PrimeLocators.AMOUNT_YEARLY_DUBAI).text.split('AED')[-1].strip()
        print("net payable amount for yearly plan in dubai is",net_payable)
        if net_payable=="95":
            print("Net payable for yearly plan is correct")
            return True
        else:
            print("Net payable is: ",net_payable)
            return False
        
    def select_yearly_plan_dubai(self):
        self.wait_for_clickable(PrimeLocators.YEARLY_PLAN_DUBAI)
        self.perform_action(PrimeLocators.YEARLY_PLAN_DUBAI)
        print("Clicked on yearly plan")
        time.sleep(1)

    def click_payment_method(self):
        self.wait_for_clickable(PrimeLocators.SELECT_PAYMENT_WAY)
        self.perform_action(PrimeLocators.SELECT_PAYMENT_WAY)
        print("Clicked on payment method")

    def check_telr_page(self,redirected_url):
        if "gateway" in redirected_url:
            print("Redirected to telr page")
            return True
        else:
            return False