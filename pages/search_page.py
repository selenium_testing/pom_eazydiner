# search_page.py

from .base_page import BasePage
from utils.locators import *
from selenium.common.exceptions import TimeoutException
import time 

class SearchPage(BasePage):

    def enter_search(self, query):
        self.wait_for_presence(SearchPageLocators.SEARCH_INPUT)
        self.perform_action(SearchPageLocators.SEARCH_INPUT, action='input', value=query)   
        print("Entered search query")
        time.sleep(2)
    
    def click_res(self, query):
        self.wait_for_visible(SearchPageLocators.RESTAURANT_SELECT(query))        
        print("Restaurant located in list")
        self.perform_action(SearchPageLocators.RESTAURANT_SELECT(query))
        print("Clicked on restaurant")
        time.sleep(6)

    def click_res_by_location(self, query):
        self.wait_for_visible(SearchPageLocators.RESTAURANT_SELECT_BY_LOCATION(query))        
        print("Restaurant located in list")
        self.perform_action(SearchPageLocators.RESTAURANT_SELECT_BY_LOCATION(query))
        print("Clicked on restaurant using location")
        time.sleep(6)
    def select_chain_res(self):
        self.wait_for_visible(SearchPageLocators.RES_CHAIN)
        self.perform_action(SearchPageLocators.RES_CHAIN)
        print("Clicked on restaurant (chain)")
        time.sleep(3)

    def check_list(self):
        try:
            self.wait_for_presence(SearchPageLocators.SEARCH_LIST)
            print("************ list showing ************")
            return True
        except TimeoutException:
            print("********* no list showing ************")
            return False
    def check_page_content(self):
        try:
            self.wait_for_presence(SearchPageLocators.SEARCH_PAGE_CONTENT)
            print("************ page content showing ************")
            return True
        except TimeoutException:
            print("********* no page content showing ************")        
            return False

    def check_recent(self,query):
        self.wait_for_visible(SearchPageLocators.SEARCH_INPUT)
        try:
            self.wait_for_visible(SearchPageLocators.RECENT(query))
            print("************ recent showing ************")
            return True
        except TimeoutException:
            print("********* recent not showing ************")
            return False
    
    def check_selected(self,title,restaurant_name):
        if title.lower()==restaurant_name.lower():
            print("************ restaurant selected ************")
            return True
        else:
            print("********* restaurant not selected ************")
            return False