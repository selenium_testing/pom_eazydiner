# test_booking.py

import sys
import os
import time

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from eazydiner_tester import EazyDinerTester

class Test_booking():

    res="Test - Do Not Book"
    res2="Cafe Hawkers"
    res1="Barbeque Nation" #paybill only after booking 
    res3="Mindspace Social" # no payeazy only bookable
    res4='Fifty9 ' 


    def test_date_slot(self):
        tester = EazyDinerTester()
        print("test_date_slot")
        tester.setup()
        tester.start()
        tester.open_search_tab()
        tester.search_restaurant(Test_booking.res)
        tester.select_restaurant(Test_booking.res)
        assert tester.is_slot_date_correct()
        tester.teardown()

    def test_time_slot(self):
        tester = EazyDinerTester()
        print("test_time_slot")
        tester.setup()
        tester.start()
        tester.open_search_tab()
        tester.search_restaurant(Test_booking.res)
        tester.select_restaurant(Test_booking.res)
        assert tester.is_slot_time_correct()
        tester.teardown()


    def test_booking(self):
        tester = EazyDinerTester()
        print("test_booking")
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("8307284752", "9021")  
        tester.open_search_tab()
        tester.search_restaurant(Test_booking.res)
        tester.select_restaurant(Test_booking.res)
        # tester.select_slot("29 May")
        # tester.click_find_best_offer()
        tester.free_book()
        tester.book()
        tester.teardown()



    def test_guest_25(self):
        tester = EazyDinerTester()
        print("test_guest_25 popup")
        tester.setup()
        tester.start()    
        tester.click_login()
        tester.login("8307284752", "9021")
        tester.open_search_tab()
        tester.search_restaurant(Test_booking.res)
        tester.select_restaurant(Test_booking.res)
        tester.open_slot_tab()
        tester.select_guest("More than 25 Guests")
        assert tester.is_guest_popup_visible()
        tester.refresh()
        tester.open_slot_tab()
        tester.select_guest("16-25 Guests")
        assert tester.is_guest_popup_visible()
        tester.teardown()

    def test_calculate_discount(self):
        tester = EazyDinerTester()
        print("test_calculate_discount")
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("8307284752", "9021")
        tester.open_search_tab()
        tester.search_restaurant(Test_booking.res4)
        tester.select_restaurant(Test_booking.res4)
        assert tester.is_calculate_discount_correct()    
        tester.teardown()

    def test_unbookable_restaurant(self):  #cafe hawkers
        tester = EazyDinerTester()
        print("test_unbookable_restaurant")
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("8307284752", "9021")
        tester.open_search_tab()
        tester.search_restaurant(Test_booking.res2)
        tester.select_res_by_location("Ambience Mall, Gurgaon")
        tester.book()
        assert tester.is_unbookable_error()
        tester.teardown()


    def test_paybill_available(self):   #mindspace social
        tester = EazyDinerTester()
        print("test_paybill_available")
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("8307284752", "9021")
        tester.location("Hyderabad")
        tester.open_search_tab()
        tester.search_restaurant(Test_booking.res3)
        tester.select_restaurant(Test_booking.res3)
        assert tester.is_redirected_payeazy()
        tester.teardown()

    def test_payeazy_bbq(self):
        tester = EazyDinerTester()
        print("test_payeazy_bbq")
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("8307284752", "9021")
        tester.open_search_tab()
        tester.search_restaurant(Test_booking.res1)
        tester.select_restaurant(Test_booking.res1)
        tester.select_chain_res()
        tester.pay_bill_bbq()
        assert tester.is_redirected_payeazy()==True
        tester.payeazy("600")
        tester.teardown()
