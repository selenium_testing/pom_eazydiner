# test_login.py

import sys
import os
import time

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from eazydiner_tester import EazyDinerTester

class Test_login:

    def test_login_successful(self):
        tester = EazyDinerTester()  
        print("test_login_successful")
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("8307284752", "9021")
        assert  tester.is_loggedin()
        tester.teardown() 

    def test_resend_otp(self):
        tester = EazyDinerTester()  
        print("test_resend_otp")
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login_enter_number("8307284752")
        tester.resend_otp()
        tester.login_enter_otp("9021")
        assert tester.is_loggedin()
        tester.teardown()

    def test_wrong_otp(self):
        tester = EazyDinerTester()  
        print("test_wrong_otp")
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("8307284752","1234")
        assert not tester.is_loggedin()
        tester.teardown()

    def test_wrong_number(self):
        tester = EazyDinerTester()  
        print("test_wrong_number")
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("830728475211","1234")
        assert not tester.is_loggedin()
        tester.teardown()

    def test_login_alphanumeric(self):
        tester = EazyDinerTester()  
        print("test_login_alphanumeric")
        tester.setup()
        tester.start()  
        tester.click_login()
        tester.login("aaa83072a84752","9021")
        assert tester.is_loggedin()
        tester.teardown()

    def test_logout(self):
        tester = EazyDinerTester()
        print("test_logout")
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("8307284752", "9021")
        tester.logout()
        assert tester.is_loggedout()
        tester.teardown()


    def test_login_url(self):
        tester = EazyDinerTester()
        print("test_login_url")
        tester.setup()
        tester.start()
        tester.go_to_url("https://www.eazydiner.com/login")
        tester.login("8307284752", "9021")
        assert tester.is_loggedin()
        tester.teardown()
 

    # def test_new_user_login(self):
    #     tester = EazyDinerTester()
    #     print("test_new_user_login")    
    #     tester.setup()
    #     tester.start()
    #     tester.click_login()
    #     tester.newUser_login("9090909090","happy","happy@gmail.com","2002-01-01")
    #     tester.teardown()
        