# test_payeazy.py

import sys
import os
import time


sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from eazydiner_tester import EazyDinerTester

class Test_payeazy():

    res="Mist"
    res1="Barbeque Nation" #paybill only after booking 
    res2="Test - Do Not Book" #check net payable
    res3="Mindspace Social" # no payeazy only bookable
    res4='Fifty9 ' 

    def test_payeazy_page_redirection(self):
        tester = EazyDinerTester()
        print("test_payeazy_redirection")
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("8307284752", "9021")
        tester.open_search_tab() 
        tester.search_restaurant(Test_payeazy.res)
        tester.select_restaurant(Test_payeazy.res)
        tester.pay_bill()
        assert tester.is_redirected_payeazy()
        tester.teardown()
    
    def test_count_available_offers(self):
        tester = EazyDinerTester()
        print("test_count_additional_offers")
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("8307284752", "9021")
        tester.open_search_tab()
        tester.search_restaurant(Test_payeazy.res)
        tester.select_restaurant(Test_payeazy.res)
        time.sleep(3)
        tester.pay_bill()
        assert tester.check_offer_count()
        tester.teardown()

    def test_juspay_payment_methods(self):
        tester = EazyDinerTester()
        print("test_juspay_elements")
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("8307284752", "9021")
        tester.open_search_tab()
        tester.search_restaurant(Test_payeazy.res2)
        tester.select_restaurant(Test_payeazy.res2)
        tester.book()
        assert tester.is_juspay_element_visible()
        tester.teardown()
   
    def test_extra_offers(self):
            tester = EazyDinerTester()
            print("test_extra_offers")
            tester.setup()
            tester.start()
            tester.click_login()
            tester.login("8307284752", "9021")
            tester.open_search_tab()
            tester.search_restaurant(Test_payeazy.res)
            tester.select_restaurant(Test_payeazy.res)
            tester.pay_bill()
            assert tester.check_extra_offers()
            tester.teardown()

    def  test_net_payable_payment_mode(self):
        tester = EazyDinerTester()
        print("test_net_payable_payment_mode")
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("8307284752", "9021")
        tester.open_search_tab()
        tester.search_restaurant(Test_payeazy.res4)
        tester.select_restaurant(Test_payeazy.res4)
        tester.pay_bill()
        assert tester.check_offers_and_payment_mode ()
        tester.teardown()


    def test_card_juspay(self):
        tester = EazyDinerTester()
        print("test_card_juspay")
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("8307284752", "9021")
        tester.open_search_tab()
        tester.search_restaurant(Test_payeazy.res)
        tester.select_restaurant(Test_payeazy.res)
        tester.pay_bill()
        assert tester.check_card_bin()
        tester.teardown()


    def test_check_convienience_fee(self):
        tester = EazyDinerTester()
        print("test_check_convienience_fee")
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("8307284752", "9021") 
        tester.open_search_tab()
        tester.search_restaurant(Test_payeazy.res)
        tester.select_restaurant(Test_payeazy.res)
        time.sleep(2)
        tester.pay_bill()
        assert tester.is_convenience_fee_correct()
        tester.teardown()

    def test_paybill_without_login(self):
        tester = EazyDinerTester()
        print("test_paybill_without_login")
        tester.setup()
        tester.start()
        tester.open_search_tab()
        tester.search_restaurant(Test_payeazy.res)
        tester.select_restaurant(Test_payeazy.res)
        tester.pay_bill_banner()
        tester.payeazy("6000")
        print("logging in")
        tester.login("8307284752", "9021")
        assert tester.is_loggedin()
        tester.payeazy("6000")
        tester.teardown()
