import sys
import os
import time

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from eazydiner_tester import EazyDinerTester

class Test_prime():
    
    res="Indian Accent"
    def test_prime(self):
        tester=EazyDinerTester()
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("7717290566","9021")
        tester.click_prime()
        assert tester.check_amount_yearly_plan()==True,"Offer amount for yearly plan is not correct."
        tester.click_proceed()
        # tester.click_cross()
        time.sleep(2)
        tester.confirm_payable_amount_yearly()
        tester.click_select_payment_option()
        assert tester.is_redirected_juspay()==True
        tester.teardown()

    def test_oneMonth_prime(self):
        tester=EazyDinerTester()
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("7717290566","9021")
        tester.click_prime()
        tester.click_one_month_plan()
        assert tester.check_amount_monthly_plan()==True,"Offer amount for one month plan is not correct."
        tester.click_proceed()
        # tester.click_cross()
        tester.confirm_payable_amount_one_month()
        tester.click_select_payment_option()
        assert tester.is_redirected_juspay()==True
        tester.teardown()

    def test_threeMonth_prime(self):
        tester=EazyDinerTester()
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("7717290566","9021")
        tester.click_prime()
        tester.click_3month_plan()
        assert tester.check_amount_three_month_plan()==True,"Offer amount for three months plan is not correct."
        tester.click_proceed()
        # tester.click_cross()
        tester.confirm_payable_amount()
        tester.click_select_payment_option()
        assert tester.is_redirected_juspay()==True
        tester.teardown()

    # def test_Nonprime_booking(self):
    #     tester=EazyDinerTester()
    #     tester.setup()
    #     tester.start()
    #     tester.click_login()
    #     tester.login("7717290566","9021")
    #     tester.open_search_tab()
    #     tester.select_restaurant(Test_prime.res)
    #     tester.teardown()

    def test_prime_prevMem(self):
        tester=EazyDinerTester()
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("7976676080","9021")
        tester.click_prime()
        assert tester.check_amount_3month_plan_prevMem()==True,"Offer amount of three months plan for previous prime user is not correct."
        tester.click_proceed()
        tester.confirm_payable_amount_prev_mem()
        tester.click_select_payment_option()
        assert tester.is_redirected_juspay()==True
        tester.teardown()

    def test_oneMonth_prime_prevMem(self):
        tester=EazyDinerTester()
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("7976676080","9021")
        tester.click_prime()
        tester.click_one_month_plan_prevMem()
        time.sleep(2)
        assert tester.check_amount_monthly_plan_prevMem()==True,"Offer amount of one month plan for previous prime user is not correct."
        tester.click_proceed()
        tester.confirm_payable_amount_one_month_prevMem()
        tester.click_select_payment_option()
        assert tester.is_redirected_juspay()==True
        tester.teardown()

    # def test_yearly_prime_prevMem(self):
    #     tester=EazyDinerTester()
    #     tester.setup()
    #     tester.start()
    #     tester.click_login()
    #     tester.login("7976676080","9021")
    #     tester.click_prime()
    #     tester.click_yearly_plan_prevMem()
    #     assert tester.check_amount_yearly_plan_prevMem()==True,"Offer amount of yearly plan for previous prime user is not correct."
    #     tester.click_proceed()
    #     tester.confirm_payable_amount_yearly_prevMem()
    #     tester.click_select_payment_option()
    #     assert tester.is_redirected_juspay()==True
    #     tester.teardown()

    def test_dubai_three_month_prime(self):
        tester=EazyDinerTester()
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("7717290566","9021")
        tester.location("Dubai")
        tester.click_prime()
        assert tester.check_amount_dubai_three_month_plan()==True,"Offer amount for Dubai three montyh plan is not correct."
        tester.click_proceed()
        assert tester.check_net_payable_3monthPlan_dubai()==True,"Three month plan net payable amount not correct in dubai."
        tester.click_select_payment_option()
        tester.select_payment_way()
        time.sleep(2)
        assert tester.is_redirected_telr()==True , "Redirection failed to secure telr page"
        tester.teardown()

    def test_dubai_yearly_prime(self):
        tester=EazyDinerTester()
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("7717290566","9021")
        tester.location("Dubai")
        tester.click_prime()
        tester.click_yearly_plan_dubai()
        assert tester.check_amount_dubai_yearly_plan()==True,"Offer amount for Dubai yearly plan is not correct."
        tester.click_proceed()
        time.sleep(3)
        assert tester.check_net_payable_yearly_Plan_dubai()==True,"Yearly plan net payable amount not correct in dubai."
        tester.click_select_payment_option()
        tester.select_payment_way()
        time.sleep(2)
        assert tester.is_redirected_telr()==True , "Redirection failed to secure telr page"
        tester.teardown()

    

    
