# test_search.py

import sys
import os

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from eazydiner_tester import EazyDinerTester

class Test_search():
    res="Test - Do Not Book"
    res1='Mist'

    def test_search_content(self):
        tester = EazyDinerTester()
        print("test_search_content")
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("8307284752", "9021")
        tester.open_search_tab()
        assert tester.is_search_content()
        tester.teardown()

    def test_search_list(self):
        tester = EazyDinerTester()
        print("test_search_list")
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("8307284752", "9021")
        tester.open_search_tab()
        tester.search_restaurant(Test_search.res)
        assert tester.is_list_showing()
        tester.teardown()
        
    
    
    def test_select_restaurant(self):
        tester = EazyDinerTester()
        print("test_select_restaurant")
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("8307284752", "9021")
        tester.open_search_tab()
        tester.search_restaurant(Test_search.res1)
        tester.select_restaurant(Test_search.res1)
        assert tester.is_selected(Test_search.res1)
        tester.teardown()

    def test_recent_restaurant(self):
        tester = EazyDinerTester()
        print("test_recent_restaurant")
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("8307284752", "9021")
        tester.open_search_tab()
        tester.search_restaurant(Test_search.res)
        tester.select_restaurant(Test_search.res)
        tester.go_to_home_page()
        tester.open_search_tab()
        assert tester.is_recent_showing(Test_search.res)
        tester.teardown()    

    def test_chain_recent(self, chain='Barbeque Nation'):
        tester = EazyDinerTester()
        print("test_chain_recent")
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("8307284752", "9021")
        tester.open_search_tab()
        tester.search_restaurant(chain)
        tester.select_restaurant(chain)
        tester.select_chain_res()
        tester.go_to_home_page()
        tester.open_search_tab()
        assert tester.is_recent_showing(chain)
        tester.teardown()
