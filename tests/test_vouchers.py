# test_vouchers.py

import sys
import os
import time

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from eazydiner_tester import EazyDinerTester

class Test_booking():

    def test_buy_voucher(self):
        tester = EazyDinerTester()
        tester.setup()
        tester.start()
        tester.click_login()
        tester.login("8307284752", "9021")
        tester.open_search_tab()
        tester.search_restaurant("Test - Do Not Book")
        tester.select_restaurant("Test - Do Not Book")
        tester.open_voucher_tab()
        tester.add_vouchers()        
        tester.teardown()
