# locators.py

from selenium.webdriver.common.by import By

class BasePageLocators:
    HOME_TAB = (By.CSS_SELECTOR, ("img[alt='EazyDiner']"))
    SEARCH_TAB = (By.XPATH, '//div[@class="relative flex search_main_home align-v-center radius-10 margin-t-20 pointer"]')
    LOGIN_BUTTON = (By.XPATH, '//div[@class="flex login_btn_home full-height font-14 white align-v-center pointer" and text()="Login"]')
    USER_BUTTON=(By.CSS_SELECTOR,'.customer_name.radius-50.flex.flex-center.align-v-center')
    PROFILE_BUTTON = (By.CSS_SELECTOR, ("img[alt='profile icon']"))
    CONTACT=(By.CSS_SELECTOR,(".pointer.grey[data-testid='contact']"))
    LOCATION=(By.CSS_SELECTOR,(".flex.full-width.v-flex-end.loc_name_aero.ellipsis"))
    PRIME=(By.CSS_SELECTOR,".pointer.grey.color_FF4612.bold")
    BOOK_A_TABLE=(By.CSS_SELECTOR,"div[class='nowrap x-auto padding-15 hide_scroll padding-t-30 padding-b-30 padding-l-0'] div:nth-child(1) div:nth-child(1)")

class LocationPageLocators(BasePageLocators):
    LOCATION_INPUT=(By.CSS_SELECTOR,("input[placeholder='Type a city to find your location']"))
    @staticmethod
    def LOCATION_SELECT(location):
        return ((By.XPATH, f"//img[@alt='{location}']"))
    CURRENT_LOCATION = (By.CSS_SELECTOR, ".white.current_loc_text.margin-l-10.font-15.semi-bold")
    
class RestaurantLocators(BasePageLocators):
    FIRST_RESTO=(By.CSS_SELECTOR,"body > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > div:nth-child(1) > div:nth-child(4) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2)")        
    ENTER_OFFER=(By.CSS_SELECTOR,"input[name='name']")
    APPLY=(By.CSS_SELECTOR,".absolute.link.pointer.checkout_apply_coupon__ab0J4")
    OFFER_ERROR=(By.CSS_SELECTOR,".red.margin-t-10")
    OK_THANKS=(By.CSS_SELECTOR,".close_thanks.padding-b-20.padding-t-20.pointer.text-center.text-blue.font-15.eazypoints_content")
    REMOVE_OFFER=(By.XPATH,"//div[@class='pointer flex']//*[name()='svg']")
    SELECT_OFFER=(By.CSS_SELECTOR,"img[alt='Accordion down aero']")
    CONVENIENCE_FEE=(By.CSS_SELECTOR,"div[class='flex flex-between black padding-b-10 padding-t-10 font-16 checkout_checkout_charge__spvX1'] div:nth-child(1)")
    CROSS=(By.XPATH,"//div[@class='flex pointer hide_mobile']//*[name()='svg']")
class ProfilePageLocators(BasePageLocators):
    ACTIVITY_BUTTON=(By.XPATH,'//div[contains(text(),"Activities")]')
    LOG_OUT=(By.XPATH, '//div[contains(text(),"Log Out")]')
    def PROFILE_BUTTONS(text):
        PROFILE_BUTTONS=(By.XPATH, f"//div[contains(text(),'{text}')]")


class LoginPageLocators(BasePageLocators):
    MOBILE_INPUT = (By.XPATH, '//input[@class="full-width float_input remove-spin-buttons" and @name="mobile"]')
    GET_OTP_BUTTON = (By.XPATH, '//button[contains(@class, "white pointer relative text-center font-15 semi-bold full-width flex-1 btns_main active_main block border_none") and contains(text(), "Get OTP")]')
    @staticmethod
    def OTP_INPUT(index):
        return (By.CSS_SELECTOR, f'div.flex.align-v-center input.full-width.float_input.otp_inputs[name="otp{index}"]')
    E_MSG=(By.XPATH,'//*[@id="__next"]/div[2]/div[1]/div[3]/div/div/div[3]/div')
    NAME_INPUT = (By.CSS_SELECTOR,'input[name="name"]')
    EMAIL_INPUT = (By.CSS_SELECTOR,'input[name="email"]')
    DOB_INPUT=(By.XPATH,'//label[normalize-space()="Date of birth"]')
    ANNIVERSARY_INPUT = (By.CLASS_NAME,'default_date_picker top-0 full-width full-height left-0 absolute login_signupDate__dSO6y')
    SIGNUP_NOW_BUTTON = (By.CSS_SELECTOR,'.white.pointer.relative.text-center.font-15.semi-bold.full-width.flex-1.btns_main.active_main.block.border_none')
    RESEND_OTP_BUTTON = (By.XPATH,'//div[@class="margin-l-5 link font-12"]')

class SearchPageLocators(BasePageLocators):
    SEARCH_INPUT = (By.CSS_SELECTOR, 'input.search_input.full-width[data-testid="input-search"]')
    def RESTAURANT_SELECT(text):
        return (By.XPATH, f'//img[@alt="{text}"]')
    def RESTAURANT_SELECT_BY_LOCATION(text):
        return (By.XPATH,f"(//div[@class='location_name font-11 more-grey'][normalize-space()='{text}'])[1]")
    
    SEARCH_LIST=(By.CSS_SELECTOR, 'div.flex.align-v-center.loc_result.margin-b-20.pointer')
    SEARCH_PAGE_CONTENT = (By.XPATH, '//div[contains(@class, "font-16") and contains(@class, "semi-bold") and contains(@class, "grey-darker")]')
    @staticmethod
    def RECENT(text):
        return (By.XPATH, f"//div[contains(@class, 'font-16') and contains(text(), 'Recent Searches')]/following-sibling::div//div[contains(@class, 'top_searched') and contains(text(), '{text}')]")
    RES_CHAIN=(By.XPATH,("(//img[@alt='Restaurant Image'])[1]"))



class BookingPageLocators(BasePageLocators):
    @staticmethod
    def RESTAURANT_NAME(name):
        return (By.XPATH, f"//div[contains(text(), '{name}')]")
    SLOT_TAB=(By.CLASS_NAME, 'restaurantDetails_date_time_box__dZABY')
    ACTIVE_TIME=By.XPATH, "//div[contains(@class, 'restaurantDetails_active_border_deal__9fsDQ')]/div/div[contains(@class, 'restaurantDetails_day_text__3tO8_')]"
    @classmethod
    def DATE_TAB(cls, text):
        return (By.XPATH, f"//div[contains(@class, 'restaurantDetails_slot_boxes')]/div[contains(text(), '{text}')]")
    @staticmethod
    def TIME_TAB(time):
        return (By.XPATH, f"//div[contains(@class, 'restaurantDetails_day_text__3tO8_') and contains(text(), '{time}')]")
    @staticmethod
    def GUEST_TAB(guest):
        return (By.XPATH, f"//div[@class='restaurantDetails_day_text__3tO8_'][normalize-space()='{guest}']")
    
    FIND_BEST_OFFER_BUTTON=(By.CLASS_NAME, "white.pointer.relative.text-center.font-15.semi-bold.full-width.flex-1.btns_main.active_main.block.border_none")
    OFFER_SECTION=(By.CLASS_NAME, "bg-white.radius-8.right_slot.restaurantDetails_show_desktop_section__trRlZ")
    NEXT_DEAL=(By.XPATH, "/html/body/div/div[1]/div[3]/div/div[2]/div[2]/div/div[2]/div/div[1]/div[4]")
    # NEXT_DEAL=(By.XPATH,"//div[@class='swiper swiper-initialized swiper-horizontal space_slider_both']//div[@class='slider_aero absolute swiper-button-next pointer active_slide']")
    BOOK_FOR_FREE = (By.CSS_SELECTOR, (".flex-1.flex.align-v-center.pointer.flex-center.padding-10.radius-10.gradient_btn.white"))
    SUBMIT_BOOKING=(By.CSS_SELECTOR, ("div[class='text-center left-0 padding-10 fixed full-width bottom-0 radius-10 bg-white listing_header'] div button[class='white pointer relative text-center font-15 semi-bold full-width flex-1 btns_main active_main block border_none']"))
    CONFIRM_BOOKING=(By.XPATH, ("//div[@class='font-16 text-normal black margin-t-5']"))
    BOOK_BUTTON=(By.CSS_SELECTOR,(".flex-1.pointer.padding-10.radius-10.gradient_btn.white"))
    PAY_BILL = (By.CSS_SELECTOR, ".flex-1.padding-10.radius-10.margin-r-10.pointer.restaurantDetails_at_restaurant__G_t_W")
    PAY_BILL_BANNER=(By.XPATH, ("//img[@alt='Pay Bill Banner']"))
    PAY_BILL_BBQ=(By.CSS_SELECTOR,"div[class='flex-1 padding-10 radius-10 margin-r-10 pointer restaurantDetails_at_restaurant__G_t_W'] div[class='orange font-15']")
    @staticmethod
    def POP_UP_OFFER_ACTIVE(text):
        return (By.XPATH, f"//div[contains(@class, 'pointer') and contains(@class, 'active') and contains(text(), '{text}')]")

    @staticmethod
    def POP_UP_OFFER_INACTIVE(text):
        return (By.XPATH, f"//div[contains(@class, 'pointer') and not(contains(@class, 'active')) and contains(text(), '{text}')]")

    def OFFER_BUTTON(text):
          return (By.XPATH, f"//div[@class='font-12 margin-b-5' and contains(text(), '{text}')]")
    
    BUY_VOUCHER=(By.XPATH, "//img[@src='https://d4t7t8y8xqo0t.cloudfront.net/app/eazymedia/icons/prepaid_voucher_rdv_offer_23_3.png']")    
    ADD_VOUCHER=(By.CSS_SELECTOR, ".text-center.padding-10.radius-5.pointer.margin-r-10.margin-b-10.max-width.margin-left-auto.gradient_btn.white.restaurantDetails_addBtn__iCvNP")
    CONTINUE_TO_BUY=(By.CSS_SELECTOR, "div.flex-1.pointer.padding-10.radius-10.white.align-v-center.flex.flex-center")
    GUEST_POPUP=(By.CSS_SELECTOR, "div.mobile_modal_content")
    SAMPLE_BILL = (By.CLASS_NAME, "padding-15.margin-10.radius-10.restaurantDetails_sample_bill__M_erI")
    DISCOUNT_CALCULATOR = (By.CSS_SELECTOR, "div.underline.link.pointer.text-right")
    AMOUNT_INPUT = (By.CSS_SELECTOR, "input.float_input.bg-grey-secondary[name='amount'][data-testid='float-input']") #sample bill
    OPEN_OFFERS=By.XPATH,(("//div[@class='flex padding-10 border_one radius-4 flex-between bg-grey-secondary align-v-center']"))
    # FLAT_OFFER=(By.XPATH,("//div[contains(@class, 'restaurantDetails_popup_offer_main__kD_fr')]//div[contains(text(), 'Flat')]//ancestor::div[contains(@class, 'restaurantDetails_popup_offer_main__kD_fr')]//div[contains(@class, 'padding-5')]/div[contains(@class, 'radius-4') and text()='Select']"))
    FLAT_DISCOUNT_OFFER = (By.XPATH, "//div[contains(@class, 'restaurantDetails_popup_offer_main__kD_fr')]//div[contains(text(), 'Flat')]/ancestor::div[contains(@class, 'restaurantDetails_popup_offer_main__kD_fr')]")
    INPUT_OFFER = (By.CSS_SELECTOR, "input[data-testid='input-search']")
    SELECT_OFFER_BUTTON = (By.XPATH, "//div[contains(@class, 'light-green') and contains(text(), 'Select')]")
    CALCULATE_DISCOUNT = (By.CSS_SELECTOR, ".white.pointer.relative.text-center.font-15.semi-bold.full-width.flex-1.btns_main.active_main.block.border_none")
    # PAYMENT_OFFER = (By.XPATH, "//div[contains(@class, 'flex-between')]//*[contains(@class, 'ellipsis') and text()='Payment Offer']")
    # RESTAURANT_OFFER = (By.XPATH, "//div[contains(@class, 'flex-between')]//*[contains(@class, 'ellipsis') and text()='Restaurant Offer']")
    RESTAURANT_DISCOUNT_VALUE = (By.XPATH, "//div[@class='flex flex-between black padding-b-10 padding-t-10 font-16 green checkout_checkout_charge__spvX1']//div[contains(text(), 'Restaurant Offer')]/following-sibling::div")
    PAYMENT_DISCOUNT_VALUE = (By.XPATH, "//div[@class='flex flex-between black padding-b-10 padding-t-10 font-16 green checkout_checkout_charge__spvX1']//div[contains(text(), 'Payment Offer')]/following-sibling::div")
    FINAL_VALUE_AFTER_DISCOUNT= (By.XPATH, "//div[@class='flex bill_amount black font-15 padding-b-10 padding-t-10 bold flex-between align-v-center bold checkout_checkout_charge__spvX1']//div[contains(text(), 'You Paid')]/following-sibling::div")
    
    RESTAURANT_NOT_AVAILABLE_MESSAGE = (By.XPATH, "//div[@class='text-center red padding-t-5 padding-b-5']")

class PayEazyLocators(BasePageLocators):
    AMOUNT_INPUT =(By.XPATH, "//input[@type='number' and @placeholder='Amount']")
    CHECK_OFFERS=(By.CSS_SELECTOR, "button.white.pointer.relative.text-center.font-15.semi-bold.full-width.flex-1.btns_main.active_main.block.border_none")
    ERROR_VALUE=(By.CSS_SELECTOR, "div.padding-15.text-center")
    DISMISS_ERROR=(By.CSS_SELECTOR, "div.white.pointer.relative.text-center.font-15.semi-bold.full-width.flex-1.btns_main.bg-error.block.border_none")
    COUPON_CODE_TAB=(By.CSS_SELECTOR, ".modal_content_main.bg-white")
    OFFER_ELEMENTS = (By.XPATH,'//div[@class="padding-l-10 black font-13"]') #FOR CSV COMPARISION
    OFFER_COUNT = (By.XPATH,'//div[contains(@class, "additional_offer")]')
    ADDITIONAL_OFFER_NAME =(By.XPATH, '//div[contains(@class, "font-14") and contains(@class, "bold") and contains(@class, "padding-b-5") and contains(@class, "grey-dark")]')
    NET_PAYABLE=(By.XPATH, "//div[@class='flex bill_amount black font-15 padding-t-20 bold flex-between align-v-center']//div[2]")
    PLATFORM_DISCOUNT = (By.XPATH,("//div[@class='flex flex-between black padding-b-10 padding-t-10 font-16 green checkout_checkout_charge__spvX1']//div[2]"))
    CONVINIENCE_FEE=(By.XPATH,("//div[@class='flex flex-between black padding-b-10 padding-t-10 font-16 checkout_checkout_charge__spvX1']//div[2]"))
    SELECT_OFFER=(By.XPATH,("(//div[@class='select_coupon link pointer font-13'][normalize-space()='Select'])[1]")) #FIRST SELECTABLE OFFER
    CLOSE_OFFERS_SECTION = (By.CSS_SELECTOR, "div.flex.pointer.hide_mobile > svg")
    #JUSPAY
    CARD_ELEMENT = (By.XPATH, "//div[@id='1000118']")
    UPI_ELEMENT = (By.XPATH, "//div[@id='1000124']")
    NET_BANKING_ELEMENT = (By.XPATH, "//div[@id='1000133']")
    WALLETS_ELEMENT = (By.XPATH, "//div[@id='1000128']")
    # PAYMENT_METHOD_AFTER_OFFER=(By.XPATH, "//article[@style='word-break: break-word; display: inline;']")
    # PAYMENT_METHOD_AFTER_OFFER=(By.CSS_SELECTOR, "#1000073 > article")
    SELECT_PAYMENT_OPTION = (By.XPATH, "//button[contains(@class, 'btns_main') and contains(., 'Select Payment Option')]")
    JUSPAY_CARD=(By.XPATH, "//article[contains(text(), 'Credit / Debit Cards')]")
    JUSPAY_UPI=(By.XPATH, "//article[contains(text(), 'UPI')]")
    JUSPAY_WALLETS=(By.XPATH, "//article[contains(text(), 'Wallets')]")
    JUSPAY_NET_BANKING=(By.XPATH, "//article[contains(text(), 'Net Banking')]")
    COUPON_APPLIED = (By.ID, "1000035")
    SELECT_CARD= (By.ID, "1000079")
    CARD_INPUT=(By.XPATH, '//input[@id="1000096"]')
    EXPIRY_INPUT=(By.ID, "1000097")
    CVV_INPUT=(By.ID, "1000098")
    # CARD_ERROR=(By.XPATH, "//article[contains(@style, 'word-break: break-word;') and contains(@style, 'display: inline;') and contains(., 'Recheck the card number')]")
    PAY_BUTTON = (By.ID, "1000191")
    CARD_ERROR=(By.XPATH, "//*[@id='1000161']")

class PrimeLocators(BasePageLocators):
    PROCEED_BUTTON=(By.CSS_SELECTOR,".white.pointer.relative.text-center.font-15.semi-bold.full-width.flex-1.btns_main.active_main.block.border_none")
    CROSS=(By.XPATH,"//*[name()='path' and contains(@d,'M16 1.622L')]")
    PAYMENT_OPTION=(By.CSS_SELECTOR,".white.pointer.relative.text-center.font-15.semi-bold.full-width.flex-1.btns_main.active_main.block.border_none")
    #payable amount without gst
    AMOUNT=(By.CSS_SELECTOR,"body > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(4) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2)")
    AMOUNT_ONE_MONTH=(By.CSS_SELECTOR,"body > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(4) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2)")
    AMOUNT_YEARLY=(By.CSS_SELECTOR,"div:nth-child(4) div:nth-child(1) div:nth-child(1) div:nth-child(2)")
    #Payable amount without gst for previous members
    AMOUNT_PREV_MEM=(By.XPATH,"//div[contains(text(),'₹423')]")
    AMOUNT_1_PREV_MEM=(By.XPATH,"//div[contains(text(),'₹169')]")
    AMOUNT_YEARLY_PREV_MEM=(By.XPATH,"//div[contains(text(),'₹1058')]")
    #Final Payable amount
    PAYABLE_AMOUNT=(By.XPATH,"//div[contains(@class,'flex bill_amount black font-15 padding-t-20 bold flex-between align-v-center')]//div[2]")
    #Final payable amount for previous members
    PAYABLE_AMOUNT_PREV_MEM=(By.CSS_SELECTOR,"div:nth-child(4) div:nth-child(1) div:nth-child(4) div:nth-child(2)")
    PAYABLE_AMOUNT_ONE_MONTH_PREV_MEM=(By.CSS_SELECTOR,"body > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(4) > div:nth-child(1) > div:nth-child(4) > div:nth-child(2)")
    PAYABLE_AMOUNT_YEARLY_PREV_MEM=(By.CSS_SELECTOR,"body > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(4) > div:nth-child(1) > div:nth-child(4) > div:nth-child(2)")
    #Plan selection
    ONE_MONTH_PLAN=(By.CSS_SELECTOR,"body > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > div:nth-child(2) > div:nth-child(3) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2)")
    YEARLY_PLAN=(By.CSS_SELECTOR,"body > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > div:nth-child(2) > div:nth-child(3) > div:nth-child(1) > div:nth-child(3) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2)")
    THREE_MONTH_PLAN=(By.CSS_SELECTOR,"body > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > div:nth-child(2) > div:nth-child(3) > div:nth-child(1) > div:nth-child(3) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2)")
    #Plan selection for previous members
    ONE_MONTH_PLAN_PREV_MEM=(By.XPATH,"//div[@class='black font-12 plan_months padding-l-5 padding-r-5 ellipsis']")
    YEARLY_PLAN_PREV_MEM=(By.CSS_SELECTOR,"body > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > div:nth-child(2) > div:nth-child(3) > div:nth-child(1) > div:nth-child(3) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2)")
    #Amount Verification on prime page
    YEARLY_PLAN_VALUE=(By.XPATH,"//div[@class='pointer flex tabs-title flex-between padding-15 font-12 grey-another-light capitalize active']//div[@class='plan_price black padding-t-5 padding-b-5']")
    MONTHLY_PLAN_VALUE=(By.CSS_SELECTOR,"body > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > div:nth-child(2) > div:nth-child(3) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2)")
    THREE_MONTH_PLAN_VALUE=(By.XPATH,"//div[contains(@class,'tabs-item-main prime_plans conatiner')]//div[3]//div[1]//div[1]//div[2]//div[2]")
    #Amount Verification on prime page for previous members
    THREE_MONTH_PLAN_VALUE_PREVMEM=(By.XPATH,"//div[@class='pointer flex tabs-title flex-between padding-15 font-12 grey-another-light capitalize active']//div[@class='plan_price black padding-t-5 padding-b-5']")
    # YEARLY_PLAN_VALUE_PREVMEM=(By.XPATH,"//div[contains(@class,'flex all-tabs')]//div[1]//div[1]//div[1]//div[2]//div[2]")
    YEARLY_PLAN_VALUE_PREVMEM=(By.XPATH,"(//div[@class='plan_price black padding-t-5 padding-b-5'])[3]")
    MONTHLY_PLAN_VALUE_PREVMEM=(By.XPATH,"//div[@class='plan_price black padding-t-5 padding-b-5']")
    #Dubai amount verification on prime_page
    VALUE_THREE_MONTH_PLAN_DUBAI=(By.XPATH,"(//div[@class='plan_price black padding-t-5 padding-b-5'])[2]")
    VALUE_YEARLY_PLAN_DUBAI=(By.XPATH,"(//div[@class='plan_price black padding-t-5 padding-b-5'])[1]")
    #Plan selection dubai 
    YEARLY_PLAN_DUBAI=(By.XPATH,"(//div[contains(@class,'padding-t-20 padding-b-10 bg-white plan_title_box ellipsis')])[1]")
    # YEARLY_PLAN_DUBAI=(By.CLASS_NAME,"padding-t-20 padding-b-10 bg-white plan_title_box ellipsis")
    #Net Payable amount Dubai
    AMOUNT_THREE_MONTH_DUBAI=(By.CSS_SELECTOR,"body > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(4) > div:nth-child(1) > div:nth-child(3) > div:nth-child(2)")
    AMOUNT_YEARLY_DUBAI=(By.CSS_SELECTOR,"div:nth-child(4) div:nth-child(1) div:nth-child(3) div:nth-child(2)")
    SELECT_PAYMENT_WAY=(By.XPATH,"//div[@class='margin-l-10 font-15']")


class OffersLocators(BasePageLocators):
    RELATIVE_COUPON_CODE=(By.XPATH,'//div[@class="padding-l-10 black font-13"]')
    INAVLID_COUPON_ERROR=(By.CSS_SELECTOR,".red.margin-t-10")

class websiteCouponCompare(BasePageLocators):
    pass